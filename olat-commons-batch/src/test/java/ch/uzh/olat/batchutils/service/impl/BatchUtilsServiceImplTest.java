/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import ch.uzh.olat.batchutils.data.entity.BatchJobExecution;
import ch.uzh.olat.batchutils.data.entity.BatchJobExecutionParams;
import ch.uzh.olat.batchutils.data.entity.BatchJobInstance;
import ch.uzh.olat.batchutils.data.entity.BatchStepExecution;
import ch.uzh.olat.batchutils.data.repository.*;
import ch.uzh.olat.utils.DateAndTimeUtil;
import java.lang.management.ManagementFactory;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@ContextConfiguration(classes = BatchUtilsServiceTestConfiguration.class)
@Sql(scripts = "classpath:BatchJobServiceImplTest_Create.sql")
@Sql(
    scripts = "classpath:BatchJobServiceImplTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class BatchUtilsServiceImplTest {

  private static final String IMPORT_SEMESTERS_STEP_NAME = "import-semesters";

  @Autowired private BatchUtilsServiceImpl batchJobServiceImpl;
  @Autowired private BatchJobInstanceRepository batchJobInstanceRepository;
  @Autowired private BatchJobExecutionRepository batchJobExecutionRepository;
  @Autowired private BatchJobExecutionParamsRepository batchJobExecutionParamsRepository;
  @Autowired private BatchJobExecutionContextRepository batchJobExecutionContextRepository;
  @Autowired private BatchStepExecutionRepository batchStepExecutionRepository;
  @Autowired private BatchStepExecutionContextRepository batchStepExecutionContextRepository;

  @Test
  void testGetStartTimeOfSpecificBatchStepByJobInstanceId() {

    BatchJobInstance batchJobInstance = batchJobInstanceRepository.findById(100008L).orElse(null);
    assertNotNull(batchJobInstance);
    BatchStepExecution batchStepExecution1 =
        batchStepExecutionRepository.findById(30081L).orElse(null);
    BatchStepExecution batchStepExecution2 =
        batchStepExecutionRepository.findById(30181L).orElse(null);
    assertNotNull(batchStepExecution1);
    assertNotNull(batchStepExecution2);
    assertEquals(
        batchJobInstance, batchStepExecution1.getBatchJobExecution().getBatchJobInstance());
    assertEquals(
        batchJobInstance, batchStepExecution2.getBatchJobExecution().getBatchJobInstance());
    assertEquals(IMPORT_SEMESTERS_STEP_NAME, batchStepExecution1.getStepName());
    assertEquals(IMPORT_SEMESTERS_STEP_NAME, batchStepExecution2.getStepName());
    assertEquals("FAILED", batchStepExecution1.getStatus());
    assertEquals("COMPLETED", batchStepExecution2.getStatus());
    assertNotNull(batchStepExecution2.getStartTime());

    LocalDateTime startTimeOfSemesterImportStep =
        batchJobServiceImpl.getStartTimeOfSpecificBatchStepByJobInstanceId(
            IMPORT_SEMESTERS_STEP_NAME, 100008L);
    assertEquals(batchStepExecution2.getStartTime(), startTimeOfSemesterImportStep);
  }

  private static LocalDateTime getJvmStartTime() {
    long jvmStartTimeInMilliseconds = ManagementFactory.getRuntimeMXBean().getStartTime();
    return DateAndTimeUtil.getLocalDateTimeFromEpochTimeInMilliseconds(jvmStartTimeInMilliseconds);
  }

  @SuppressWarnings("java:S5961")
  @Test
  void
      testCheckIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed_batchStepExecutionEndTimeNull() {

    // Check end time, status, exit code and exit message before execution of method
    BatchJobExecution batchJobExecution =
        batchJobExecutionRepository.findById(200005L).orElse(null);
    assertNotNull(batchJobExecution);
    assertTrue(batchJobExecution.getStartTime().isBefore(getJvmStartTime()));
    assertNotNull(batchJobExecution.getEndTime());
    assertEquals("UNKNOWN", batchJobExecution.getStatus());
    assertEquals("UNKNOWN", batchJobExecution.getExitCode());
    assertNull(batchJobExecution.getExitMessage());

    BatchStepExecution batchStepExecution1 =
        batchStepExecutionRepository.findById(30051L).orElse(null);
    BatchStepExecution batchStepExecution2 =
        batchStepExecutionRepository.findById(30052L).orElse(null);
    BatchStepExecution batchStepExecution3 =
        batchStepExecutionRepository.findById(30053L).orElse(null);
    assertNotNull(batchStepExecution1);
    assertNotNull(batchStepExecution2);
    assertNotNull(batchStepExecution3);
    assertEquals(batchJobExecution, batchStepExecution1.getBatchJobExecution());
    assertEquals(batchJobExecution, batchStepExecution2.getBatchJobExecution());
    assertEquals(batchJobExecution, batchStepExecution3.getBatchJobExecution());
    assertNotNull(batchStepExecution1.getEndTime());
    assertNotNull(batchStepExecution2.getEndTime());
    assertNull(batchStepExecution3.getEndTime());
    assertEquals("COMPLETED", batchStepExecution1.getStatus());
    assertEquals("COMPLETED", batchStepExecution2.getStatus());
    assertEquals("STARTED", batchStepExecution3.getStatus());
    assertEquals("COMPLETED", batchStepExecution1.getExitCode());
    assertEquals("COMPLETED", batchStepExecution2.getExitCode());
    assertEquals("EXECUTING", batchStepExecution3.getExitCode());
    assertNull(batchStepExecution1.getExitMessage());
    assertNull(batchStepExecution2.getExitMessage());
    assertNull(batchStepExecution3.getExitMessage());

    // Set status to failed if required
    batchJobServiceImpl.checkIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed("job-a");

    // Check end time, status, exit code and exit message after execution of method
    batchJobExecution = batchJobExecutionRepository.findById(200005L).orElse(null);
    assertNotNull(batchJobExecution);
    assertNotNull(batchJobExecution.getEndTime());
    assertEquals(batchStepExecution2.getEndTime(), batchJobExecution.getEndTime());
    assertEquals("FAILED", batchJobExecution.getStatus());
    assertEquals("FAILED", batchJobExecution.getExitCode());
    assertNotNull(batchJobExecution.getExitMessage());

    batchStepExecution1 = batchStepExecutionRepository.findById(30051L).orElse(null);
    batchStepExecution2 = batchStepExecutionRepository.findById(30052L).orElse(null);
    batchStepExecution3 = batchStepExecutionRepository.findById(30053L).orElse(null);
    assertNotNull(batchStepExecution1);
    assertNotNull(batchStepExecution2);
    assertNotNull(batchStepExecution3);
    assertNotNull(batchStepExecution1.getEndTime());
    assertNotNull(batchStepExecution2.getEndTime());
    assertNotNull(batchStepExecution3.getEndTime());
    assertEquals("COMPLETED", batchStepExecution1.getStatus());
    assertEquals("COMPLETED", batchStepExecution2.getStatus());
    assertEquals("FAILED", batchStepExecution3.getStatus());
    assertEquals("COMPLETED", batchStepExecution1.getExitCode());
    assertEquals("COMPLETED", batchStepExecution2.getExitCode());
    assertEquals("FAILED", batchStepExecution3.getExitCode());
    assertNull(batchStepExecution1.getExitMessage());
    assertNull(batchStepExecution2.getExitMessage());
    assertNotNull(batchStepExecution3.getExitMessage());
    assertEquals(batchStepExecution2.getEndTime(), batchStepExecution3.getEndTime());
  }

  @Test
  void
      testCheckIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed_batchJobExecutionEndTimeNull() {

    // Check end time, status, exit code and exit message before execution of method
    BatchJobExecution batchJobExecution =
        batchJobExecutionRepository.findById(200006L).orElse(null);
    assertNotNull(batchJobExecution);
    assertTrue(batchJobExecution.getStartTime().isBefore(getJvmStartTime()));
    assertNull(batchJobExecution.getEndTime());
    assertEquals("STARTED", batchJobExecution.getStatus());
    assertEquals("EXECUTING", batchJobExecution.getExitCode());
    assertNull(batchJobExecution.getExitMessage());

    // Set status to failed if required
    batchJobServiceImpl.checkIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed("job-a");

    // Check end time, status, exit code and exit message after execution of method
    batchJobExecution = batchJobExecutionRepository.findById(200006L).orElse(null);
    assertNotNull(batchJobExecution);
    assertNotNull(batchJobExecution.getEndTime());
    assertEquals(batchJobExecution.getStartTime(), batchJobExecution.getEndTime());
    assertEquals("FAILED", batchJobExecution.getStatus());
    assertEquals("FAILED", batchJobExecution.getExitCode());
    assertNotNull(batchJobExecution.getExitMessage());
  }

  @Test
  void
      testCheckIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed_wrongJobName_nothingToUpdate() {

    // Check end time, status, exit code and exit message before execution of method
    BatchJobExecution batchJobExecution =
        batchJobExecutionRepository.findById(200006L).orElse(null);
    assertNotNull(batchJobExecution);
    assertTrue(batchJobExecution.getStartTime().isBefore(getJvmStartTime()));
    assertNull(batchJobExecution.getEndTime());
    assertEquals("STARTED", batchJobExecution.getStatus());
    assertEquals("EXECUTING", batchJobExecution.getExitCode());
    assertNull(batchJobExecution.getExitMessage());

    // Set status to failed if required
    batchJobServiceImpl.checkIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed("job-b");

    // Check end time, status, exit code and exit message after execution of method
    batchJobExecution = batchJobExecutionRepository.findById(200006L).orElse(null);
    assertNotNull(batchJobExecution);
    assertNull(batchJobExecution.getEndTime());
    assertEquals("STARTED", batchJobExecution.getStatus());
    assertEquals("EXECUTING", batchJobExecution.getExitCode());
    assertNull(batchJobExecution.getExitMessage());
  }

  @Test
  void
      testCheckIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed_afterJvmStartTime_nothingToUpdate() {

    // Check end time, status, exit code and exit message before execution of method
    BatchJobExecution batchJobExecution =
        batchJobExecutionRepository.findById(200007L).orElse(null);
    assertNotNull(batchJobExecution);
    assertNotNull(batchJobExecution.getStartTime());
    assertFalse(batchJobExecution.getStartTime().isBefore(getJvmStartTime()));
    assertNull(batchJobExecution.getEndTime());
    assertEquals("STARTED", batchJobExecution.getStatus());
    assertEquals("EXECUTING", batchJobExecution.getExitCode());
    assertNull(batchJobExecution.getExitMessage());

    // Set status to failed if required
    batchJobServiceImpl.checkIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed("job-a");

    // Check end time, status, exit code and exit message after execution of method
    batchJobExecution = batchJobExecutionRepository.findById(200007L).orElse(null);
    assertNotNull(batchJobExecution);
    assertNull(batchJobExecution.getEndTime());
    assertEquals("STARTED", batchJobExecution.getStatus());
    assertEquals("EXECUTING", batchJobExecution.getExitCode());
    assertNull(batchJobExecution.getExitMessage());
  }

  @SuppressWarnings("java:S5961")
  @Test
  void testDeleteBatchTableEntriesOlderThan() {

    // Batch table entries before deletion
    assertTrue((batchJobInstanceRepository.findById(100001L).isPresent()));
    assertTrue((batchJobInstanceRepository.findById(100002L).isPresent()));
    assertTrue((batchJobInstanceRepository.findById(100003L).isPresent()));
    assertTrue((batchJobInstanceRepository.findById(100004L).isPresent()));

    assertTrue((batchJobExecutionRepository.findById(200001L).isPresent()));
    assertTrue((batchJobExecutionRepository.findById(200002L).isPresent()));
    assertTrue((batchJobExecutionRepository.findById(200003L).isPresent()));
    assertTrue((batchJobExecutionRepository.findById(200004L).isPresent()));

    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200001L, "timestamp")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200002L, "a-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200002L, "b-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200003L, "a-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200003L, "b-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200004L, "a-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200004L, "b-param")));

    assertTrue((batchJobExecutionContextRepository.findById(200001L).isPresent()));
    assertTrue((batchJobExecutionContextRepository.findById(200002L).isPresent()));
    assertTrue((batchJobExecutionContextRepository.findById(200003L).isPresent()));
    assertTrue((batchJobExecutionContextRepository.findById(200004L).isPresent()));

    assertTrue((batchStepExecutionRepository.findById(30011L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30012L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30013L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30021L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30022L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30023L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30031L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30032L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30033L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30041L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30042L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30043L).isPresent()));

    assertTrue((batchStepExecutionContextRepository.findById(30011L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30012L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30013L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30021L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30022L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30023L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30031L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30032L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30033L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30041L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30042L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30043L).isPresent()));

    // Delete batch table entries
    batchJobServiceImpl.deleteBatchTableEntriesOlderThan(
        "job-b", LocalDateTime.of(2021, Month.FEBRUARY, 23, 16, 0, 0));

    // Batch table entries after deletion
    assertTrue((batchJobInstanceRepository.findById(100001L).isPresent()));
    assertFalse((batchJobInstanceRepository.findById(100002L).isPresent()));
    assertFalse((batchJobInstanceRepository.findById(100003L).isPresent()));
    assertTrue((batchJobInstanceRepository.findById(100004L).isPresent()));

    assertTrue((batchJobExecutionRepository.findById(200001L).isPresent()));
    assertFalse((batchJobExecutionRepository.findById(200002L).isPresent()));
    assertFalse((batchJobExecutionRepository.findById(200003L).isPresent()));
    assertTrue((batchJobExecutionRepository.findById(200004L).isPresent()));

    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200001L, "timestamp")));
    assertNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200002L, "a-param")));
    assertNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200002L, "b-param")));
    assertNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200003L, "a-param")));
    assertNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200003L, "b-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200004L, "a-param")));
    assertNotNull(
        batchJobExecutionParamsRepository.findById(
            new BatchJobExecutionParams.PrimaryKey(200004L, "b-param")));

    assertTrue((batchJobExecutionContextRepository.findById(200001L).isPresent()));
    assertFalse((batchJobExecutionContextRepository.findById(200002L).isPresent()));
    assertFalse((batchJobExecutionContextRepository.findById(200003L).isPresent()));
    assertTrue((batchJobExecutionContextRepository.findById(200004L).isPresent()));

    assertTrue((batchStepExecutionRepository.findById(30011L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30012L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30013L).isPresent()));
    assertFalse((batchStepExecutionRepository.findById(30021L).isPresent()));
    assertFalse((batchStepExecutionRepository.findById(30022L).isPresent()));
    assertFalse((batchStepExecutionRepository.findById(30023L).isPresent()));
    assertFalse((batchStepExecutionRepository.findById(30031L).isPresent()));
    assertFalse((batchStepExecutionRepository.findById(30032L).isPresent()));
    assertFalse((batchStepExecutionRepository.findById(30033L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30041L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30042L).isPresent()));
    assertTrue((batchStepExecutionRepository.findById(30043L).isPresent()));

    assertTrue((batchStepExecutionContextRepository.findById(30011L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30012L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30013L).isPresent()));
    assertFalse((batchStepExecutionContextRepository.findById(30021L).isPresent()));
    assertFalse((batchStepExecutionContextRepository.findById(30022L).isPresent()));
    assertFalse((batchStepExecutionContextRepository.findById(30023L).isPresent()));
    assertFalse((batchStepExecutionContextRepository.findById(30031L).isPresent()));
    assertFalse((batchStepExecutionContextRepository.findById(30032L).isPresent()));
    assertFalse((batchStepExecutionContextRepository.findById(30033L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30041L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30042L).isPresent()));
    assertTrue((batchStepExecutionContextRepository.findById(30043L).isPresent()));
  }
}
