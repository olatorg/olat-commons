-- The Spring batch tables (created by BatchJobServiceImplTest_Create.sql) must be deleted again
-- after the @SpringBoot tests have been executed. Otherwise, the @SpringBatch tests will fail,
-- because @SpringBatch tests with the configuration spring.batch.jdbc.initialize-schema = always
-- assume that the Spring batch tables do not exist yet.
DROP TABLE batch_step_execution_context;
DROP TABLE batch_step_execution;
DROP TABLE batch_job_execution_params;
DROP TABLE batch_job_execution_context;
DROP TABLE batch_job_execution;
DROP TABLE batch_job_instance;
