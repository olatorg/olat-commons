/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.entity;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * Access to Spring batch table by JPA.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Entity
@Table(name = "batch_step_execution")
@Getter
@Setter
public class BatchStepExecution {

  @Id
  @Column(name = "step_execution_id")
  private long stepExecutionId;

  @ManyToOne
  @JoinColumn(name = "job_execution_id", nullable = false)
  private BatchJobExecution batchJobExecution;

  @Column(name = "step_name", nullable = false)
  private String stepName;

  @Column(name = "create_time", nullable = false)
  private LocalDateTime createTime;

  @Column(name = "start_time")
  private LocalDateTime startTime;

  @Column(name = "end_time")
  private LocalDateTime endTime;

  @Column(name = "status")
  private String status;

  @Column(name = "exit_code")
  private String exitCode;

  @Column(name = "exit_message")
  private String exitMessage;

  @Column(name = "read_count")
  private Integer readCount;

  @Column(name = "filter_count")
  private Integer filterCount;

  @Column(name = "write_count")
  private Integer writeCount;

  @Column(name = "read_skip_count")
  private Integer readSkipCount;

  @Column(name = "process_skip_count")
  private Integer processSkipCount;

  @Column(name = "write_skip_count")
  private Integer writeSkipCount;

  @Column(name = "commit_count")
  private Integer commitCount;

  @Column(name = "rollback_count")
  private Integer rollbackCount;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BatchStepExecution that = (BatchStepExecution) o;
    return stepExecutionId == that.stepExecutionId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(stepExecutionId);
  }
}
