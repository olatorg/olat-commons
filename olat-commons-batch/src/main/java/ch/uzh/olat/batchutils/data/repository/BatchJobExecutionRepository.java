/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.repository;

import ch.uzh.olat.batchutils.data.entity.BatchJobExecution;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface BatchJobExecutionRepository extends JpaRepository<BatchJobExecution, Long> {

  @Query(
      "SELECT bje.jobExecutionId FROM BatchJobExecution bje "
          + "WHERE bje.batchJobInstance.jobInstanceId IN (:jobInstanceId)")
  List<Long> findJobExecutionsIdsByJobInstanceIdIn(
      @Param("jobInstanceId") List<Long> jobInstanceIds);

  @SuppressWarnings("unused") // used by olat-lms
  @Query(
      "SELECT bje FROM BatchJobExecution bje "
          + "WHERE bje.batchJobInstance.jobName = :jobName "
          + "AND bje.batchJobInstance.jobKey = :jobKey")
  List<BatchJobExecution> findByJobNameAndJobKey(
      @Param("jobName") String jobName, @Param("jobKey") String jobKey);

  @Query(
      "SELECT DISTINCT COUNT(bje.jobExecutionId) FROM BatchJobExecution bje "
          + "WHERE bje.batchJobInstance.jobName = :jobName "
          + "AND bje.startTime IS NOT NULL AND bje.endTime IS NULL")
  long countBatchJobExecutionsByJobNameAndStartTimeIsNotNullAndEndTimeIsNull(
      @Param("jobName") String jobName);

  @Query(
      "SELECT DISTINCT bje FROM BatchJobExecution bje "
          + "LEFT JOIN FETCH bje.batchStepExecutions "
          + "WHERE bje.batchJobInstance.jobName = :jobName "
          + "AND bje.startTime IS NOT NULL "
          + "AND (bje.endTime IS NULL "
          + "OR EXISTS (SELECT bse FROM BatchStepExecution bse "
          + "WHERE bse.batchJobExecution.jobExecutionId = bje.jobExecutionId "
          + "AND bse.endTime IS NULL)) "
          + "ORDER BY bje.startTime DESC")
  List<BatchJobExecution> findByJobNameAndStartTimeIsNotNullAndEndTimeIsNullOrderByStartTimeDesc(
      @Param("jobName") String jobName);

  @Query(
      "SELECT DISTINCT bje FROM BatchJobExecution bje "
          + "LEFT JOIN FETCH bje.batchStepExecutions "
          + "WHERE bje.batchJobInstance.jobName = :jobName "
          + "AND bje.startTime < :startTimeReference "
          + "AND (bje.endTime IS NULL "
          + "OR EXISTS (SELECT bse FROM BatchStepExecution bse "
          + "WHERE bse.batchJobExecution.jobExecutionId = bje.jobExecutionId "
          + "AND bse.endTime IS NULL))")
  List<BatchJobExecution> findByJobNameAndStartTimeBeforeAndEndTimeIsNull(
      @Param("jobName") String jobName,
      @Param("startTimeReference") LocalDateTime startTimeReference);

  @SuppressWarnings("unused") // used by olat-lms
  @Query(
      "SELECT MAX(bje.startTime) FROM BatchJobExecution bje "
          + "WHERE bje.batchJobInstance.jobName = :jobName")
  LocalDateTime findStartTimeOfLatestBatchJobByJobName(@Param("jobName") String jobName);

  @Modifying
  @Query("DELETE FROM BatchJobExecution bje WHERE bje.jobExecutionId IN (:jobExecutionIds)")
  void deleteByJobExecutionIdIn(@Param("jobExecutionIds") List<Long> jobExecutionIds);
}
