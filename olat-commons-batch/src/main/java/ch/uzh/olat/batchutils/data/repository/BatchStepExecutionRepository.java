/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.repository;

import ch.uzh.olat.batchutils.data.entity.BatchStepExecution;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface BatchStepExecutionRepository extends JpaRepository<BatchStepExecution, Long> {

  @Query(
      "SELECT bse.stepExecutionId FROM BatchStepExecution bse "
          + "WHERE bse.batchJobExecution.jobExecutionId IN (:jobExecutionIds)")
  List<Long> findStepExecutionsIdsByJobExecutionIdIn(
      @Param("jobExecutionIds") List<Long> jobExecutionIds);

  @Query(
      "SELECT bse.startTime FROM BatchStepExecution bse "
          + "WHERE bse.batchJobExecution.batchJobInstance.jobInstanceId = :jobInstanceId "
          + "AND bse.stepName = :stepName "
          + "AND bse.status = :status ORDER BY bse.startTime ASC")
  List<LocalDateTime> findStartTimesByJobInstanceIdAndStepNameAndStatusOrderByStartTimeAsc(
      @Param("jobInstanceId") long jobInstanceId,
      @Param("stepName") String stepName,
      @Param("status") String status);

  @SuppressWarnings("unused") // used by olat-lms
  @Query(
      "SELECT bse FROM BatchStepExecution bse "
          + "JOIN FETCH bse.batchJobExecution bje "
          + "WHERE bje.batchJobInstance.jobName = :jobName "
          + "AND bje.startTime > :startTime "
          + "ORDER BY bse.stepExecutionId DESC")
  List<BatchStepExecution> findByJobNameAndStartTimeAfter(
      @Param("jobName") String jobName, @Param("startTime") LocalDateTime startTime);

  @SuppressWarnings("unused") // used by olat-lms
  @Query(
      "SELECT MAX(bse.startTime) FROM BatchStepExecution bse "
          + "WHERE bse.batchJobExecution.batchJobInstance.jobName = :jobName "
          + "AND bse.stepName = :stepName")
  LocalDateTime findStartTimeOfLatestBatchStepOfType(
      @Param("jobName") String jobName, @Param("stepName") String stepName);

  @Modifying
  @Query("DELETE FROM BatchStepExecution bse WHERE bse.stepExecutionId IN (:stepExecutionIds)")
  void deleteByStepExecutionIdIn(@Param("stepExecutionIds") List<Long> stepExecutionIds);
}
