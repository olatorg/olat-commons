/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.service.impl;

import ch.uzh.olat.batchutils.data.entity.BatchJobExecution;
import ch.uzh.olat.batchutils.data.entity.BatchStepExecution;
import ch.uzh.olat.batchutils.data.repository.*;
import ch.uzh.olat.batchutils.service.BatchUtilsService;
import ch.uzh.olat.utils.DateAndTimeUtil;
import ch.uzh.olat.utils.QueryUtil;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
@Slf4j
public class BatchUtilsServiceImpl implements BatchUtilsService {

  private static final String COMPLETED = "COMPLETED";
  private static final String FAILED = "FAILED";

  private final BatchJobInstanceRepository batchJobInstanceRepository;
  private final BatchJobExecutionRepository batchJobExecutionRepository;
  private final BatchJobExecutionParamsRepository batchJobExecutionParamsRepository;
  private final BatchJobExecutionContextRepository batchJobExecutionContextRepository;
  private final BatchStepExecutionRepository batchStepExecutionRepository;
  private final BatchStepExecutionContextRepository batchStepExecutionContextRepository;
  private final DataSource dataSource;

  public BatchUtilsServiceImpl(
      BatchJobInstanceRepository batchJobInstanceRepository,
      BatchJobExecutionRepository batchJobExecutionRepository,
      BatchJobExecutionParamsRepository batchJobExecutionParamsRepository,
      BatchJobExecutionContextRepository batchJobExecutionContextRepository,
      BatchStepExecutionRepository batchStepExecutionRepository,
      BatchStepExecutionContextRepository batchStepExecutionContextRepository,
      DataSource dataSource) {
    this.batchJobInstanceRepository = batchJobInstanceRepository;
    this.batchJobExecutionRepository = batchJobExecutionRepository;
    this.batchJobExecutionParamsRepository = batchJobExecutionParamsRepository;
    this.batchJobExecutionContextRepository = batchJobExecutionContextRepository;
    this.batchStepExecutionRepository = batchStepExecutionRepository;
    this.batchStepExecutionContextRepository = batchStepExecutionContextRepository;
    this.dataSource = dataSource;
  }

  @Override
  @Transactional(readOnly = true)
  public LocalDateTime getStartTimeOfSpecificBatchStepByJobInstanceId(
      String batchStepName, long jobInstanceId) {
    List<LocalDateTime> startTimes =
        batchStepExecutionRepository
            .findStartTimesByJobInstanceIdAndStepNameAndStatusOrderByStartTimeAsc(
                jobInstanceId, batchStepName, COMPLETED);
    return (startTimes.isEmpty()) ? null : startTimes.get(0);
  }

  /**
   * This method uses a similar DB query as called by JobExplorer#findRunningJobExecutions. In
   * contrast to JobExplorer#findRunningJobExecutions, this method uses the Hikari connection pool
   * to get a DB connection. -> Avoid risk that getting DB session could fail due to exhausted DB
   * session pool.
   */
  @Override
  @Transactional(readOnly = true)
  public boolean checkIfThereAreRunningBatchJobs(String jobName) {
    return batchJobExecutionRepository
            .countBatchJobExecutionsByJobNameAndStartTimeIsNotNullAndEndTimeIsNull(jobName)
        > 0;
  }

  @Override
  @Transactional
  public void setLatestRunningBatchJobToFailed(String jobName, String exitMessage) {

    // If the batch_job_execution table does not exist, the Spring Batch tables do not have been
    // generated yet. -> Nothing needs to be done.
    if (!checkIfTableExists(null, "batch_job_execution")) {
      return;
    }

    List<BatchJobExecution> batchJobExecutionsOfRunningJobs =
        batchJobExecutionRepository
            .findByJobNameAndStartTimeIsNotNullAndEndTimeIsNullOrderByStartTimeDesc(jobName);
    if (batchJobExecutionsOfRunningJobs.isEmpty()) {
      return;
    }

    // Set latest running batch job to failed
    setBatchJobsToFailed(List.of(batchJobExecutionsOfRunningJobs.get(0)), exitMessage);
  }

  @Override
  @Transactional
  public void checkIfBatchJobStoppedDueToServerShutdownExistsAndSetItToFailed(String jobName) {

    // If the batch_job_execution table does not exist, the Spring Batch tables do not have been
    // generated yet. -> Nothing needs to be done.
    if (!checkIfTableExists(null, "batch_job_execution")) {
      return;
    }

    // JVM start time, see https://stackoverflow.com/questions/817801/time-since-jvm-started
    long jvmStartTimeInMilliseconds = ManagementFactory.getRuntimeMXBean().getStartTime();

    List<BatchJobExecution> batchJobExecutions =
        batchJobExecutionRepository.findByJobNameAndStartTimeBeforeAndEndTimeIsNull(
            jobName,
            DateAndTimeUtil.getLocalDateTimeFromEpochTimeInMilliseconds(
                jvmStartTimeInMilliseconds));

    setBatchJobsToFailed(batchJobExecutions, "Failed due to server shutdown");
  }

  @SuppressWarnings("BooleanMethodIsAlwaysInverted")
  private boolean checkIfTableExists(
      @SuppressWarnings("SameParameterValue") String schemaName,
      @SuppressWarnings("SameParameterValue") String tableName) {
    try (Connection connection = dataSource.getConnection()) {
      // Check if table exists, see https://www.baeldung.com/jdbc-check-table-exists
      DatabaseMetaData databaseMetaData = connection.getMetaData();
      ResultSet resultSet =
          databaseMetaData.getTables(null, schemaName, tableName, new String[] {"TABLE"});
      return resultSet.next();
    } catch (SQLException e) {
      log.warn(
          "Could not determine if table {} exists --> assuming no. Reason: {}",
          tableName,
          e.getMessage());
      return false;
    }
  }

  private void setBatchJobsToFailed(
      List<BatchJobExecution> batchJobExecutions, String exitMessage) {

    for (BatchJobExecution batchJobExecution : batchJobExecutions) {
      log.info("Setting batch job execution {} to failed.", batchJobExecution.getJobExecutionId());

      // Get latest step start time
      LocalDateTime latestStepStartTime = batchJobExecution.getStartTime();
      for (BatchStepExecution batchStepExecution : batchJobExecution.getBatchStepExecutions()) {
        LocalDateTime stepStartTime = batchStepExecution.getStartTime();
        if (stepStartTime != null && stepStartTime.isAfter(latestStepStartTime)) {
          latestStepStartTime = stepStartTime;
        }
      }

      batchJobExecution.setEndTime(latestStepStartTime);
      batchJobExecution.setStatus(FAILED);
      batchJobExecution.setExitCode(FAILED);
      batchJobExecution.setExitMessage(exitMessage);

      for (BatchStepExecution batchStepExecution : batchJobExecution.getBatchStepExecutions()) {
        if (!batchStepExecution.getStatus().equals(COMPLETED)) {
          batchStepExecution.setEndTime(latestStepStartTime);
          batchStepExecution.setStatus(FAILED);
          batchStepExecution.setExitCode(FAILED);
          batchStepExecution.setExitMessage(exitMessage);
        }
      }
    }
  }

  @Override
  @Transactional
  public void deleteBatchTableEntriesOlderThan(String jobName, LocalDateTime referenceDate) {

    // Ids of job instances to be deleted
    List<Long> idsOfJobInstancesToBeDeleted =
        batchJobInstanceRepository.findJobInstanceIdsByJobNameAndJobExecutionStartTimeBefore(
            jobName, referenceDate);
    if (idsOfJobInstancesToBeDeleted.isEmpty()) {
      return;
    }

    // Ids of job executions to be deleted
    List<Long> idsOfJobExecutionsToBeDeleted = new ArrayList<>();
    for (List<Long> chunkOfIdsOfJobInstancesToBeDeleted :
        QueryUtil.collectionOfChunks(idsOfJobInstancesToBeDeleted)) {
      List<Long> chunkOfIdsOfJobExecutionsToBeDeleted =
          batchJobExecutionRepository.findJobExecutionsIdsByJobInstanceIdIn(
              chunkOfIdsOfJobInstancesToBeDeleted);
      idsOfJobExecutionsToBeDeleted.addAll(chunkOfIdsOfJobExecutionsToBeDeleted);
    }

    // Ids of step executions to be deleted
    List<Long> idsOfStepExecutionsToBeDeleted = new ArrayList<>();
    for (List<Long> chunkOfIdsOfJobExecutionsToBeDeleted :
        QueryUtil.collectionOfChunks(idsOfJobExecutionsToBeDeleted)) {
      List<Long> chunkOfIdsOfStepExecutionsToBeDeleted =
          batchStepExecutionRepository.findStepExecutionsIdsByJobExecutionIdIn(
              chunkOfIdsOfJobExecutionsToBeDeleted);
      idsOfStepExecutionsToBeDeleted.addAll(chunkOfIdsOfStepExecutionsToBeDeleted);
    }

    // Delete entries of batch tables
    for (List<Long> chunkOfIdsOfStepExecutionsToBeDeleted :
        QueryUtil.collectionOfChunks(idsOfStepExecutionsToBeDeleted)) {
      batchStepExecutionContextRepository.deleteByStepExecutionIdIn(
          chunkOfIdsOfStepExecutionsToBeDeleted);
      batchStepExecutionRepository.deleteByStepExecutionIdIn(chunkOfIdsOfStepExecutionsToBeDeleted);
    }

    for (List<Long> chunkOfIdsOfJobExecutionsToBeDeleted :
        QueryUtil.collectionOfChunks(idsOfJobExecutionsToBeDeleted)) {
      batchJobExecutionContextRepository.deleteByJobExecutionIdIn(
          chunkOfIdsOfJobExecutionsToBeDeleted);
      batchJobExecutionParamsRepository.deleteByJobExecutionIdIn(
          chunkOfIdsOfJobExecutionsToBeDeleted);
      batchJobExecutionRepository.deleteByJobExecutionIdIn(chunkOfIdsOfJobExecutionsToBeDeleted);
    }

    for (List<Long> chunkOfIdsOfJobInstancesToBeDeleted :
        QueryUtil.collectionOfChunks(idsOfJobInstancesToBeDeleted)) {
      batchJobInstanceRepository.deleteByJobInstanceIdIn(chunkOfIdsOfJobInstancesToBeDeleted);
    }
  }
}
