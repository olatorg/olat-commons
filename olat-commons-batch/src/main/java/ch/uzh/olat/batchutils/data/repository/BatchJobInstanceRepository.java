/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.repository;

import ch.uzh.olat.batchutils.data.entity.BatchJobInstance;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface BatchJobInstanceRepository extends JpaRepository<BatchJobInstance, Long> {

  @Query(
      "SELECT bji.jobInstanceId FROM BatchJobInstance bji "
          + "WHERE bji.jobName = :jobName "
          + "AND EXISTS (SELECT bje FROM BatchJobExecution bje "
          + "WHERE bje.batchJobInstance.jobInstanceId = bji.jobInstanceId "
          + "AND bje.startTime < :referenceDate)")
  List<Long> findJobInstanceIdsByJobNameAndJobExecutionStartTimeBefore(
      @Param("jobName") String jobName, @Param("referenceDate") LocalDateTime referenceDate);

  @Modifying
  @Query("DELETE FROM BatchJobInstance bji WHERE bji.jobInstanceId IN (:jobInstanceIds)")
  void deleteByJobInstanceIdIn(@Param("jobInstanceIds") List<Long> jobInstanceIds);
}
