/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.entity;

import jakarta.persistence.*;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * Access to Spring batch table by JPA.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Entity
@Table(name = "batch_job_execution_context")
@Getter
@Setter
public class BatchJobExecutionContext {

  @Id
  @Column(name = "job_execution_id", nullable = false)
  private long jobExecutionId;

  @OneToOne
  @PrimaryKeyJoinColumn(name = "job_execution_id", referencedColumnName = "job_execution_id")
  private BatchJobExecution batchJobExecution;

  @Column(name = "short_context", nullable = false)
  private String shortContext;

  @Column(name = "serialized_context")
  private String serializedContext;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BatchJobExecutionContext that = (BatchJobExecutionContext) o;
    return jobExecutionId == that.jobExecutionId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(jobExecutionId);
  }
}
