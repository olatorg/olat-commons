/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.entity;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

/**
 * Access to Spring batch table by JPA.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Entity
@Table(name = "batch_job_instance")
@Getter
@Setter
public class BatchJobInstance {

  @Id
  @Column(name = "job_instance_id")
  private long jobInstanceId;

  @OneToMany(mappedBy = "batchJobInstance")
  private final Set<BatchJobExecution> batchJobExecutions = new HashSet<>();

  @Column(name = "job_name", nullable = false)
  private String jobName;

  @Column(name = "job_key", nullable = false)
  private String jobKey;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BatchJobInstance that = (BatchJobInstance) o;
    return jobInstanceId == that.jobInstanceId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(jobInstanceId);
  }
}
