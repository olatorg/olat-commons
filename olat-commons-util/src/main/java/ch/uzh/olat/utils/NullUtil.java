/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.utils;

import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Initial date: 2017-01-17<br>
 *
 * @author sev26 (UZH)
 * @since 1.0
 */
@SuppressWarnings("unused") // used by zak
public class NullUtil {

  private NullUtil() {}

  public static <T> T notNullOrFail(@Nullable T object) {
    if (object == null) {
      throw new IllegalArgumentException("@AssumeAssertion(nullness)");
    }
    return object;
  }

  public static String notNullOrEmpty(@Nullable String s) {
    return s == null ? "" : s;
  }
}
