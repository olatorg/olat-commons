/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.junit.jupiter.api.Test;

/**
 * @author Martin Schraner
 * @since 1.0
 */
class DateAndTimeUtilTest {

  @Test
  void getLocalDateTimeAsDate() {
    LocalDateTime localDateTime = LocalDateTime.of(2021, Month.FEBRUARY, 11, 18, 9, 27);
    Date date = DateAndTimeUtil.getLocalDateTimeAsDate(localDateTime);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    assertEquals("2021-02-11 18:09:27", simpleDateFormat.format(date));
  }

  @Test
  void getDateAsLocalDateTime() {
    Date date = new GregorianCalendar(2021, Calendar.FEBRUARY, 11, 18, 9, 27).getTime();
    LocalDateTime localDateTime = DateAndTimeUtil.getDateAsLocalDateTime(date);
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    assertEquals("2021-02-11 18:09:27", localDateTime.format(dateTimeFormatter));
  }

  @Test
  void getLocalDateTimeFromEpochTimeInMilliseconds() {
    Date date = new GregorianCalendar(2021, Calendar.FEBRUARY, 11, 18, 9, 27).getTime();
    long timeInMilliseconds = date.getTime();
    LocalDateTime localDateTime =
        DateAndTimeUtil.getLocalDateTimeFromEpochTimeInMilliseconds(timeInMilliseconds);
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    assertEquals("2021-02-11 18:09:27", localDateTime.format(dateTimeFormatter));
  }
}
