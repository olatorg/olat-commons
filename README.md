# OLAT Commons

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![OpenOlat](https://img.shields.io/badge/OpenOlat-19.1--SNAPSHOT-1aa6be)](https://gitlab.com/olatorg/OpenOLAT/-/tree/OLAT_19.1)
[![Spring Boot](https://img.shields.io/badge/Spring_Boot-3.4.0-6bb536)](https://docs.spring.io/spring-boot/3.4/index.html)

Collection of reusable Java components for OLAT

## Usage

1. Add this repository to the `repositories` section of your project's `pom.xml`.

   ```xml
   <repositories>
     <repository>
       <id>olatorg-repo</id>
       <url>https://gitlab.com/api/v4/groups/olatorg/-/packages/maven</url>
     </repository>
   </repositories>
   ```

2. Add this dependency to the `dependencyManagement` section.

   ```xml
   <dependencyManagement>
     <dependencies>
       <dependency>
         <groupId>org.olat</groupId>
         <artifactId>olat-commons-dependencies</artifactId>
         <version>${olat-commons.version}</version>
         <type>pom</type>
         <scope>import</scope>
       </dependency>
     </dependencies>
   </dependencyManagement>
   ```

3. Add the corresponding dependency of the [module you need](#modules) to the `dependency` section.

   ```xml
   <dependency>
     <groupId>org.olat</groupId>
     <artifactId>olat-commons-util</artifactId>
   </dependency>
   ```

## Modules

| Module                | Description                                                                | Maven coordinates                |
|-----------------------|----------------------------------------------------------------------------|----------------------------------|
| OLAT Commons Util     | Common util classes                                                        | `org.olat:olat-commons-util`     |
| OLAT Commons Batch    | Common classes for [Spring Batch](https://spring.io/projects/spring-batch) | `org.olat:olat-commons-batch`    |
| OLAT Commons OpenOlat | Common classes for OpenOlat                                                | `org.olat:olat-commons-openolat` |

## Development

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olat-commons.git
   cd olat-commons
   ```

2. Create a local PostgreSQL database (e.g. with [Docker](https://docs.docker.com/get-started/)).

   ```shell
   docker run --name olat-test-db --env POSTGRES_DB=olattest --env POSTGRES_USER=olat --env POSTGRES_PASSWORD=olat -p 5432:5432 -v olat-test-db_data:/var/lib/postgresql/data --detach postgres:12-alpine
   ```

3. Build project.

   ```shell
   ./mvnw verify
   ```

## License

This project is Open Source software released under
the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).
