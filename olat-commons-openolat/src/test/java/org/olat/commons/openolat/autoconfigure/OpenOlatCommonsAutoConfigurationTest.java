/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.commons.openolat.autoconfigure;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Random;
import org.junit.jupiter.api.Test;
import org.olat.core.CoreSpringInitializer;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.assertj.AssertableWebApplicationContext;
import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@Testcontainers
class OpenOlatCommonsAutoConfigurationTest {

  private static final String[] BEAN_NAMES = {
    "groupMembershipDao", "groupMembershipRepository", "groupMembershipServiceOlatTransactionImpl",
  };

  @Container
  @SuppressWarnings("resource")
  private final PostgreSQLContainer<?> postgresql =
      new PostgreSQLContainer<>("postgres:12-alpine")
          .withDatabaseName("olattest")
          .withUsername("olat")
          .withPassword("olat");

  private final WebApplicationContextRunner contextRunner =
      new WebApplicationContextRunner()
          .withConfiguration(AutoConfigurations.of(OpenOlatCommonsAutoConfiguration.class))
          .withInitializer(new CoreSpringInitializer())
          .withAllowBeanDefinitionOverriding(true)
          .withAllowCircularReferences(true);

  @Test
  void contextLoads() {
    contextRunner
        .withSystemProperties("db.host=" + postgresql.getHost())
        .withSystemProperties("db.host.port=" + postgresql.getFirstMappedPort())
        .withSystemProperties("userdata.dir=${java.io.tmpdir}/olatdata/" + new Random().nextInt())
        .run(
            context -> {
              assertThat(context).hasNotFailed();
              assertHasBeans(context);
            });
  }

  private void assertHasBeans(AssertableWebApplicationContext context) {
    for (String beanName : BEAN_NAMES) {
      assertThat(context).hasBean(beanName);
    }
  }
}
