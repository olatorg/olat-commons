/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATRepositoryTestConfiguration.class)
@Sql(scripts = "classpath:GroupMembershipRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class GroupMembershipRepositoryTest {

  @Autowired private GroupMembershipRepository groupMembershipRepository;

  @Test
  void testFindByGroupKeyAndRoleWithEagerLoading() {
    List<GroupMembershipImpl> groupMemberships =
        groupMembershipRepository.findByGroupKeyAndRoleWithEagerLoading(5555552L, "author");
    assertEquals(2, groupMemberships.size());
    List<Long> identityKeys =
        groupMemberships.stream()
            .map(groupMembership -> groupMembership.getIdentity().getKey())
            .toList();
    assertTrue(identityKeys.contains(2222222L));
    assertTrue(identityKeys.contains(2222223L));
  }

  @Test
  void testFindByGroupKeyAndIdentityKeyAndRole() {
    Optional<GroupMembershipImpl> groupMembershipOptional1 =
        groupMembershipRepository.findByGroupKeyAndIdentityKeyAndRole(5555551L, 2222221L, "author");
    assertTrue(groupMembershipOptional1.isPresent());
    assertEquals(6666662L, (long) groupMembershipOptional1.get().getKey());
    Optional<GroupMembershipImpl> groupMembershipOptional2 =
        groupMembershipRepository.findByGroupKeyAndIdentityKeyAndRole(5555551L, 2222222L, "author");
    assertFalse(groupMembershipOptional2.isPresent());
  }

  @Test
  void testFindByGroupKeyAndIdentityKeyInAndRole() {
    List<Long> identityKeys = List.of(2222221L, 2222222L, 2222223L, 2222224L);
    List<GroupMembershipImpl> groupMemberships =
        groupMembershipRepository.findByGroupKeyAndIdentityKeyInAndRole(
            5555552L, identityKeys, "author");
    assertEquals(2, groupMemberships.size());
    List<Long> groupMembershipKeys =
        groupMemberships.stream().map(GroupMembershipImpl::getKey).toList();
    assertTrue(groupMembershipKeys.contains(6666666L));
    assertTrue(groupMembershipKeys.contains(6666668L));
  }
}
