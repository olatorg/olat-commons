/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATRepositoryTestConfiguration.class)
@Sql(scripts = "classpath:SimpleIdentityRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class SimpleIdentityRepositoryTest {

  @Autowired private SimpleIdentityRepository simpleIdentityRepository;

  @Test
  void testFindById() {
    Optional<SimpleIdentity> simpleIdentityOptional = simpleIdentityRepository.findById(2222221);
    assertTrue(simpleIdentityOptional.isPresent());
    assertEquals("u111111", simpleIdentityOptional.get().getName());
  }

  @Test
  void testFindByUserIdsAndStatusLt() {
    List<Long> userIds = new ArrayList<>();
    userIds.add(3333331L);
    userIds.add(3333333L);
    userIds.add(3333334L);
    List<SimpleIdentity> simpleIdentities =
        simpleIdentityRepository.findByUserIdsAndStatusLt(userIds, 7);
    assertEquals(2, simpleIdentities.size());
    List<Long> identityIds = simpleIdentities.stream().map(SimpleIdentity::getId).toList();
    assertTrue(identityIds.contains(2222221L));
    assertTrue(identityIds.contains(2222224L));
  }
}
