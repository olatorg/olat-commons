/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 2.1
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATDaoTestConfiguration.class)
@Sql(scripts = "classpath:GroupMembershipRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class GroupMembershipDaoTest {

  @Autowired private GroupMembershipDao groupMembershipDao;

  @Test
  void testFindByGroupKeyAndRoleWithEagerLoading() {
    List<GroupMembershipImpl> groupMemberships =
        groupMembershipDao.findByGroupKeyAndRoleWithEagerLoading(5555552L, "author");
    assertEquals(2, groupMemberships.size());
    List<Long> identityKeys =
        groupMemberships.stream()
            .map(groupMembership -> groupMembership.getIdentity().getKey())
            .toList();
    assertTrue(identityKeys.contains(2222222L));
    assertTrue(identityKeys.contains(2222223L));
  }

  @Test
  void testFindByGroupKeyAndIdentityKeyInAndRole() {
    List<Long> identityKeys = List.of(2222221L, 2222222L, 2222223L, 2222224L);
    List<GroupMembershipImpl> groupMemberships =
        groupMembershipDao.findByGroupKeyAndIdentityKeyInAndRole(5555552L, identityKeys, "author");
    assertEquals(2, groupMemberships.size());
    List<Long> groupMembershipKeys =
        groupMemberships.stream().map(GroupMembershipImpl::getKey).toList();
    assertTrue(groupMembershipKeys.contains(6666666L));
    assertTrue(groupMembershipKeys.contains(6666668L));
  }
}
