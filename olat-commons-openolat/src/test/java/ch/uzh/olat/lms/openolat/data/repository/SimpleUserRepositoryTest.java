/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATRepositoryTestConfiguration.class)
@Sql(scripts = "classpath:SimpleUserRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class SimpleUserRepositoryTest {

  @Autowired private SimpleUserRepository simpleUserRepository;

  @Test
  void testFindByIdentityId() {
    Optional<SimpleUser> simpleUserOptional = simpleUserRepository.findByIdentityId(2222222);
    assertTrue(simpleUserOptional.isPresent());
    assertEquals("mkappenberger", simpleUserOptional.get().getNickname());
  }

  @Test
  void testFindNicknameByIdentityIdIn() {
    Optional<String> nicknameOptional = simpleUserRepository.findNicknameByIdentityId(2222222);
    assertTrue(nicknameOptional.isPresent());
    assertEquals("mkappenberger", nicknameOptional.get());
  }

  @Test
  void testFindByInstitutionalUserIdentifierLikeAndInstitutionalName() {
    List<SimpleUser> simpleUsers =
        simpleUserRepository.findByInstitutionalUserIdentifierLikeAndInstitutionalName(
            "11111%", "uzh.ch");
    assertEquals(3, simpleUsers.size());
    List<Long> userIds = simpleUsers.stream().map(SimpleUser::getId).toList();
    assertTrue(userIds.contains(3333331L));
    assertTrue(userIds.contains(3333332L));
    assertTrue(userIds.contains(3333333L));
  }

  @Test
  void testFindByInstitutionalUserIdentifierLikeOrEmailAndInstitutionalName() {
    List<SimpleUser> simpleUsers =
        simpleUserRepository.findByInstitutionalUserIdentifierLikeOrEmailAndInstitutionalName(
            "111111%", "max.kappeler@foo.ch", "uzh.ch");
    assertEquals(3, simpleUsers.size());
    List<Long> userIds = simpleUsers.stream().map(SimpleUser::getId).toList();
    assertTrue(userIds.contains(3333331L));
    assertTrue(userIds.contains(3333332L));
    assertTrue(userIds.contains(3333334L));
  }

  @Test
  void testFindByEmployeeNumberLikeOrInOrEmailInAndInstitutionalName() {
    List<String> employeeNumbers = Collections.singletonList("88882");
    List<String> emails = Collections.singletonList("max.kappeler@foo.ch");
    List<SimpleUser> simpleUsers =
        simpleUserRepository.findByEmployeeNumberLikeOrInOrEmailInAndInstitutionalName(
            "88888%", employeeNumbers, emails, "uzh.ch");
    assertEquals(3, simpleUsers.size());
    List<Long> userIds = simpleUsers.stream().map(SimpleUser::getId).toList();
    assertTrue(userIds.contains(3333331L));
    assertTrue(userIds.contains(3333333L));
    assertTrue(userIds.contains(3333334L));
  }

  @Test
  void testFindByEmployeeNumberLikeOrInAndInstitutionalName() {
    List<String> employeeNumbers = Collections.singletonList("88882");
    List<SimpleUser> simpleUsers =
        simpleUserRepository.findByEmployeeNumberLikeOrInAndInstitutionalName(
            "88888%", employeeNumbers, "uzh.ch");
    assertEquals(2, simpleUsers.size());
    List<Long> userIds = simpleUsers.stream().map(SimpleUser::getId).toList();
    assertTrue(userIds.contains(3333331L));
    assertTrue(userIds.contains(3333333L));
  }

  @Test
  void testFindByEmployeeNumberLikeOrEmailInAndInstitutionalName() {
    List<String> emails = Collections.singletonList("max.kappeler@foo.ch");
    List<SimpleUser> simpleUsers =
        simpleUserRepository.findByEmployeeNumberLikeOrEmailInAndInstitutionalName(
            "88888%", emails, "uzh.ch");
    assertEquals(2, simpleUsers.size());
    List<Long> userIds = simpleUsers.stream().map(SimpleUser::getId).toList();
    assertTrue(userIds.contains(3333331L));
    assertTrue(userIds.contains(3333334L));
  }

  @Test
  void testFindByEmployeeNumberLikeAndInstitutionalName() {
    List<SimpleUser> simpleUsers =
        simpleUserRepository.findByEmployeeNumberLikeAndInstitutionalName("8888%", "uzh.ch");
    assertEquals(3, simpleUsers.size());
    List<Long> userIds = simpleUsers.stream().map(SimpleUser::getId).toList();
    assertTrue(userIds.contains(3333331L));
    assertTrue(userIds.contains(3333333L));
    assertTrue(userIds.contains(3333333L));
  }

  @Test
  void testFindIdentityIdAndNicknamesByIdentityIdIn() {
    List<Long> identityIds = List.of(2222221L, 2222222L, 2222224L);
    List<IdentityIdAndNickname> identityIdsAndNicknames =
        simpleUserRepository.findIdentityIdAndNicknamesByIdentityIdIn(identityIds);
    assertEquals(3, identityIdsAndNicknames.size());
    identityIdsAndNicknames.sort(Comparator.comparing(IdentityIdAndNickname::identityId));
    assertEquals(2222221L, identityIdsAndNicknames.get(0).identityId());
    assertEquals("mkapp", identityIdsAndNicknames.get(0).nickname());
    assertEquals(2222222L, identityIdsAndNicknames.get(1).identityId());
    assertEquals("mkappenberger", identityIdsAndNicknames.get(1).nickname());
    assertEquals(2222224L, identityIdsAndNicknames.get(2).identityId());
    assertEquals("mkappeler", identityIdsAndNicknames.get(2).nickname());
  }
}
