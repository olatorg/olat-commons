/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import ch.uzh.olat.lms.openolat.model.CourseType;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.olat.repository.RepositoryEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATDaoTestConfiguration.class)
@Sql(scripts = "classpath:RepositoryEntryRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class RepositoryEntryDaoTest {

  private static final String COURSE_MODULE = "CourseModule";
  private static final String PUBLISHED = "published";
  private static final String AUTHOR = "author";

  @Autowired private RepositoryEntryDao repositoryEntryDao;

  @Test
  void testFindById() {
    Optional<RepositoryEntry> repositoryEntryOptional1 = repositoryEntryDao.findById(7777771L);
    assertTrue(repositoryEntryOptional1.isPresent());
    assertEquals(31L, (long) repositoryEntryOptional1.get().getOlatResource().getKey());

    Optional<RepositoryEntry> repositoryEntryOptional2 = repositoryEntryDao.findById(999999L);
    assertFalse(repositoryEntryOptional2.isPresent());
  }

  @Test
  void testFindByIdWithEagerLoadingOfOlatResource() {
    Optional<RepositoryEntry> repositoryEntryOptional =
        repositoryEntryDao.findByIdWithEagerLoadingOfOlatResource(7777771L);
    assertTrue(repositoryEntryOptional.isPresent());
    assertEquals(31L, (long) repositoryEntryOptional.get().getOlatResource().getKey());
  }

  @Test
  void testFindByIdWithEagerLoadingOfRepositoryEntryLifecycle() {
    Optional<RepositoryEntry> repositoryEntryOptional =
        repositoryEntryDao.findByIdWithEagerLoadingOfRepositoryEntryLifecycle(7777771L);
    assertTrue(repositoryEntryOptional.isPresent());
    assertEquals(21L, (long) repositoryEntryOptional.get().getLifecycle().getKey());
  }

  @Test
  void testFindByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId() {
    List<RepositoryEntry> repositoryEntries =
        repositoryEntryDao.findByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
            COURSE_MODULE,
            CourseType.CONVENTIONAL.getTechnicalType(),
            List.of(PUBLISHED),
            "author",
            2222222L);
    assertEquals(2, repositoryEntries.size());
    List<Long> repositoryEntryIds =
        repositoryEntries.stream().map(RepositoryEntry::getKey).toList();
    assertTrue(repositoryEntryIds.contains(7777771L));
    assertTrue(repositoryEntryIds.contains(7777777L));
  }

  @Test
  void testFindRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId() {
    List<Long> repositoryEntryIds =
        repositoryEntryDao
            .findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
                COURSE_MODULE,
                List.of(CourseType.CONVENTIONAL.getTechnicalType()),
                List.of(PUBLISHED),
                "author",
                2222222L);
    assertEquals(2, repositoryEntryIds.size());
    assertTrue(repositoryEntryIds.contains(7777771L));
    assertTrue(repositoryEntryIds.contains(7777777L));
  }

  @Test
  void testFindByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId() {
    List<RepositoryEntry> repositoryEntries =
        repositoryEntryDao.findByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
            COURSE_MODULE, List.of(PUBLISHED), AUTHOR, 2222222L);
    assertEquals(1, repositoryEntries.size());
    List<Long> repositoryEntryIds =
        repositoryEntries.stream().map(RepositoryEntry::getKey).toList();
    assertTrue(repositoryEntryIds.contains(7777778L));
  }
}
