/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

import ch.uzh.olat.lms.openolat.data.dao.SimpleUserDao;
import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@ExtendWith(MockitoExtension.class)
class SimpleUserServiceOlatTransactionImplTest {

  @Mock private SimpleUserDao simpleUserDao;

  private SimpleUserServiceOlatTransactionImpl simpleUserServiceOlatTransaction;

  @BeforeEach
  void setUp() {
    simpleUserServiceOlatTransaction = new SimpleUserServiceOlatTransactionImpl(simpleUserDao);
  }

  @Test
  void testGetMapOfIdentityIdsAndNotEmptyNicknamesByIdentityIds() {
    // Mock DB query
    List<IdentityIdAndNickname> identityIdAndNicknames =
        List.of(
            new IdentityIdAndNickname(1L, "hhuber"),
            new IdentityIdAndNickname(2L, "fmoser"),
            new IdentityIdAndNickname(3L, "rspeich"),
            new IdentityIdAndNickname(4L, ""),
            new IdentityIdAndNickname(5L, "mkuster"),
            new IdentityIdAndNickname(6L, null));
    when(simpleUserDao.findIdentityIdAndNicknamesByIdentityIdIn(
            argThat(list -> list.containsAll(List.of(1L, 2L, 3L, 4L, 5L, 6L)))))
        .thenReturn(identityIdAndNicknames);

    Map<Long, String> mapOfIdentityIdsAndUserNames =
        simpleUserServiceOlatTransaction.getMapOfIdentityIdsAndNotEmptyNicknamesByIdentityIds(
            List.of(1L, 2L, 3L, 4L, 5L, 6L));

    Map<Long, String> expectedMap = new HashMap<>();
    expectedMap.put(1L, "hhuber");
    expectedMap.put(2L, "fmoser");
    expectedMap.put(3L, "rspeich");
    expectedMap.put(5L, "mkuster");
    assertEquals(expectedMap, mapOfIdentityIdsAndUserNames);
  }
}
