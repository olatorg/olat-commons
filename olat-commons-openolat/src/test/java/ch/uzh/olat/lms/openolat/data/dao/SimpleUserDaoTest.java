/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATDaoTestConfiguration.class)
@Sql(scripts = "classpath:SimpleUserRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class SimpleUserDaoTest {

  @Autowired private SimpleUserDao simpleUserDao;

  @Test
  void testFindByIdentityId() {
    Optional<SimpleUser> simpleUserOptional = simpleUserDao.findByIdentityId(2222222);
    assertTrue(simpleUserOptional.isPresent());
    assertEquals("mkappenberger", simpleUserOptional.get().getNickname());
  }

  @Test
  void testFindNicknameByIdentityId() {
    Optional<String> nicknameOptional = simpleUserDao.findNicknameByIdentityId(2222222);
    assertTrue(nicknameOptional.isPresent());
    assertEquals("mkappenberger", nicknameOptional.get());
  }

  @Test
  void testFindIdentityIdAndNicknamesByIdentityIdIn() {
    List<Long> identityIds = List.of(2222221L, 2222222L, 2222224L);
    List<IdentityIdAndNickname> identityIdsAndNicknames =
        simpleUserDao.findIdentityIdAndNicknamesByIdentityIdIn(identityIds);
    assertEquals(3, identityIdsAndNicknames.size());
    identityIdsAndNicknames.sort(Comparator.comparing(IdentityIdAndNickname::identityId));
    assertEquals(2222221L, identityIdsAndNicknames.get(0).identityId());
    assertEquals("mkapp", identityIdsAndNicknames.get(0).nickname());
    assertEquals(2222222L, identityIdsAndNicknames.get(1).identityId());
    assertEquals("mkappenberger", identityIdsAndNicknames.get(1).nickname());
    assertEquals(2222224L, identityIdsAndNicknames.get(2).identityId());
    assertEquals("mkappeler", identityIdsAndNicknames.get(2).nickname());
  }
}
