/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.olat.repository.model.RepositoryEntryToGroupRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SpringBootTest
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OpenOLATRepositoryTestConfiguration.class)
@Sql(scripts = "classpath:RepositoryEntryToGroupRelationRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:OpenOLATRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class RepositoryEntryToGroupRelationRepositoryTest {

  @Autowired
  private RepositoryEntryToGroupRelationRepository repositoryEntryToGroupRelationRepository;

  @Test
  void testFindByRepositoryEntryIdAndDefaultGroup() {
    List<RepositoryEntryToGroupRelation> repositoryEntryToGroupRelations =
        repositoryEntryToGroupRelationRepository.findByRepositoryEntryIdAndDefaultGroup(
            7777774L, false);
    assertEquals(2, repositoryEntryToGroupRelations.size());
    List<Long> repositoryEntryToGroupRelationKeys =
        repositoryEntryToGroupRelations.stream()
            .map(RepositoryEntryToGroupRelation::getKey)
            .toList();
    assertTrue(repositoryEntryToGroupRelationKeys.contains(888885L));
    assertTrue(repositoryEntryToGroupRelationKeys.contains(888886L));
  }
}
