INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222221, 1, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'mkapp', 3);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222222, 1, '2020-08-15 12:00:00', '2021-03-01 17:00:00', 'mkappenberger', 5);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222223, 1, '2020-08-15 12:00:00', '2021-02-01 20:00:00', 'mkaderli', 7);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222224, 1, '2020-08-15 12:00:00', '2021-01-11 11:00:00', 'mkappeler', 1);

INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555551, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555552, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555553, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555554, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555555, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555556, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555557, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555558, '2020-08-15 12:00:00', null);

INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (11, '2020-08-15 12:00:00', '2021-04-01 12:00:00', null, 0, 0, 1, 0, '2021-04-01 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (12, '2020-08-15 12:00:00', '2021-04-04 12:00:00', null, 0, 0, 8, 0, '2021-04-04 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (13, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 3, 0, '2021-04-06 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (14, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 2, 0, '2021-04-06 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (16, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 2, 0, '2021-04-06 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (17, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 2, 0, '2021-04-06 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (18, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 2, 0, '2021-04-06 12:00:00');

INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (21, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-19 00:00:00', '2020-05-28 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (22, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-20 00:00:00', '2020-05-29 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (23, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (24, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (26, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (27, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (28, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');

INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (31, 0, '2020-08-15 12:00:00', 'CourseModule', 7777771);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (32, 0, '2020-08-15 12:00:00', 'CourseModule', 7777772);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (33, 0, '2020-08-15 12:00:00', 'Exam', 7777773);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (34, 0, '2020-08-15 12:00:00', 'CourseModule', 7777774);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (36, 0, '2020-08-15 12:00:00', 'CourseModule', 7777776);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (37, 0, '2020-08-15 12:00:00', 'CourseModule', 7777777);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (38, 0, '2020-08-15 12:00:00', 'CourseModule', 7777778);

INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777771, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777771', 'condition', 'Testkurs 1',
           '-', 11, 21, 31, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777772, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777772', 'condition', 'Testkurs 2',
           '-', 12, 22, 32, 'mkappenberger', 'atAnyTime', false, false, false, false, 'preparation', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777773, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777773', 'condition', 'Exam 1',
           '-', 13, 23, 33, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777774, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777774', 'condition', 'Testkurs 3',
           '-', 14, 24, 34, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777776, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777776', 'learningpath', 'Testkurs 6',
       '-', 16, 26, 36, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777777, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777777', 'condition', 'Testkurs 7',
       '-', 17, 27, 37, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, technical_type, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777778, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777778', null, 'Testkurs 8 with technical type null',
       '-', 18, 28, 38, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);

INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888881, '2020-08-15 12:00:00', true, 5555551, 7777771);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888882, '2020-08-15 12:00:00', true, 5555552, 7777772);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888883, '2020-08-15 12:00:00', true, 5555553, 7777773);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888884, '2020-08-15 12:00:00', true, 5555554, 7777774);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888885, '2020-08-15 12:00:00', false, 5555555, 7777774);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888886, '2020-08-15 12:00:00', true, 5555556, 7777776);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888887, '2020-08-15 12:00:00', true, 5555557, 7777777);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888888, '2020-08-15 12:00:00', true, 5555558, 7777778);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666661, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555551, 2222221);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666662, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555551, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666663, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555551, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666664, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555551, 2222224);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666665, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555552, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666666, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555552, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666667, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555552, 2222223);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666668, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555552, 2222223);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666669, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555552, 2222224);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666670, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555553, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666671, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555553, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666672, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555554, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666673, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555555, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666674, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555555, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666675, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555556, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666676, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555556, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666677, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555557, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666678, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555557, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666679, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555558, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666680, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555558, 2222222);
