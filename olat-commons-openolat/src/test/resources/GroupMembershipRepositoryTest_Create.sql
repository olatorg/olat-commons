INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222221, 1, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'mkapp', 3);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222222, 1, '2020-08-15 12:00:00', '2021-03-01 17:00:00', 'mkappenberger', 5);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222223, 1, '2020-08-15 12:00:00', '2021-02-01 20:00:00', 'mkaderli', 7);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222224, 1, '2020-08-15 12:00:00', '2021-01-11 11:00:00', 'mkappeler', 1);

INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555551, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555552, '2020-08-15 12:00:00', null);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666661, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555551, 2222221);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666662, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555551, 2222221);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666663, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555551, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666664, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555551, 2222224);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666665, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555552, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666666, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555552, 2222222);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666667, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555552, 2222223);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666668, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'author', 'root', 5555552, 2222223);
INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id, fk_identity_id)
    VALUES (6666669, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'user', 'none', 5555552, 2222224);
