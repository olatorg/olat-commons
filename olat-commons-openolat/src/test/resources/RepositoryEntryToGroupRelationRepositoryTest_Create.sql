INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555551, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555552, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555553, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555554, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555555, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555556, '2020-08-15 12:00:00', null);

INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (11, '2020-08-15 12:00:00', '2021-04-01 12:00:00', null, 0, 0, 1, 0, '2021-04-01 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (12, '2020-08-15 12:00:00', '2021-04-04 12:00:00', null, 0, 0, 8, 0, '2021-04-04 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (13, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 3, 0, '2021-04-06 12:00:00');
INSERT INTO o_repositoryentry_stats(id, creationdate, lastmodified, r_rating, r_num_of_ratings, r_num_of_comments, r_launchcounter, r_downloadcounter, r_lastusage)
    VALUES (14, '2020-08-15 12:00:00', '2021-04-06 12:00:00', null, 0, 0, 2, 0, '2021-04-06 12:00:00');

INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (21, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-19 00:00:00', '2020-05-28 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (22, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-20 00:00:00', '2020-05-29 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (23, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');
INSERT INTO o_repositoryentry_cycle(id, creationdate, lastmodified, r_softkey, r_label, r_privatecycle, r_validfrom, r_validto)
    VALUES (24, '2020-08-15 12:00:00', '2020-08-15 12:00:00', 'FS 2020', 'Frühjahrssemester 2020', true, '2020-02-21 00:00:00', '2020-05-30 00:00:00');

INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (31, 0, '2020-08-15 12:00:00', 'CourseModule', 1111111111);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (32, 0, '2020-08-15 12:00:00', 'CourseModule', 1111111112);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (33, 0, '2020-08-15 12:00:00', 'Exam', 1111111113);
INSERT INTO o_olatresource(resource_id, version, creationdate, resname, resid)
    VALUES (34, 0, '2020-08-15 12:00:00', 'CourseModule', 1111111114);

INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777771, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777771', 'Testkurs 1',
           '-', 11, 21, 31, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777772, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777772', 'Testkurs 2',
           '-', 12, 22, 32, 'mkappenberger', 'atAnyTime', false, false, false, false, 'preparation', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777773, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777773', 'Exam 1',
           '-', 13, 23, 33, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);
INSERT INTO o_repositoryentry(repositoryentry_id, version, lastmodified, creationdate, softkey, displayname,
                              resourcename, fk_stats, fk_lifecycle, fk_olatresource, initialauthor, allowtoleave,
                              candownload, cancopy, canreference, canindexmetadata, status, deletiondate, fk_deleted_by)
    VALUES(7777774, 0, '2020-08-15 12:00:00', '2021-04-01 12:00:00', 'olat_1_7777774', 'Testkurs 3',
           '-', 14, 24, 34, 'mkappenberger', 'atAnyTime', false, false, false, false, 'published', null, null);

INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888881, '2020-08-15 12:00:00', true, 5555551, 7777771);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888882, '2020-08-15 12:00:00', true, 5555552, 7777772);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888883, '2020-08-15 12:00:00', true, 5555553, 7777773);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888884, '2020-08-15 12:00:00', true, 5555554, 7777774);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888885, '2020-08-15 12:00:00', false, 5555555, 7777774);
INSERT INTO o_re_to_group(id, creationdate, r_defgroup, fk_group_id, fk_entry_id)
    VALUES(888886, '2020-08-15 12:00:00', false, 5555556, 7777774);
