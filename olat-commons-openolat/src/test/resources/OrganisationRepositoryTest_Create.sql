INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555551, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555552, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555553, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555554, '2020-08-15 12:00:00', null);
INSERT INTO o_bs_group(id, creationdate, g_name) VALUES (5555555, '2020-08-15 12:00:00', null);

INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname, o_m_path_keys, o_status, fk_group, fk_root, fk_parent)
    VALUES (1001, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'default-org-test', 'OLAT', '/1001/', 'active', 5555551, null, null);
INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname, o_m_path_keys, o_status, fk_group, fk_root, fk_parent)
    VALUES (1002, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'uzh.ch-test', 'uzh.ch', '/1001/1002/', 'active', 5555552, 1001, 1001);
INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname, o_m_path_keys, o_status, fk_group, fk_root, fk_parent)
    VALUES (1003, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'careum-test', 'careum', '/1001/1003/', 'inactive', 5555553, 1001, 1001);
INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname, o_m_path_keys, o_status, fk_group, fk_root, fk_parent)
    VALUES (1004, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'unilu-test', 'unilu', '/1001/1004/', 'deleted', 5555554, 1001, 1001);
INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname, o_m_path_keys, o_status, fk_group, fk_root, fk_parent)
    VALUES (1005, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'mels-test', 'mels', '/1001/1002/1005/', 'active', 5555555, 1001, 1002);

