INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222221, 1, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'u3333331', 3);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222222, 1, '2020-08-15 12:00:00', '2021-03-01 17:00:00', 'u3333332', 5);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222223, 1, '2020-08-15 12:00:00', '2021-02-01 20:00:00', 'u3333333', 7);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222224, 1, '2020-08-15 12:00:00', '2021-01-11 11:00:00', 'u3333334', 1);

INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_nickname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333331, 1, '2020-08-15 12:00:00', false, 'Max', 'Kapp', 'mkapp', 'max.kapp@foo.ch',
    'uzh.ch', 111111111, 888888, 2222221);
INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_nickname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333332, 1, '2020-08-15 12:00:00', false, 'Martin', 'Kappenberger', 'mkappenberger', 'martin.kappenberger@foo.ch',
            'uzh.ch', 11111122, 88881, 2222222);
INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_nickname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333333, 1, '2020-08-15 12:00:00', false, 'Martina', 'Kaderli', 'mkaderli', 'martina.kaderli@foo.ch',
            'uzh.ch', 11111333, 88882, 2222223);
INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_nickname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333334, 1, '2020-08-15 12:00:00', false, 'Max', 'Kappeler', 'mkappeler', 'max.kappeler@foo.ch',
            'uzh.ch', 11114444, null, 2222224);
