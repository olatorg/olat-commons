INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222221, 1, '2020-08-15 12:00:00', '2021-03-04 18:00:00', 'u111111', 3);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222222, 1, '2020-08-15 12:00:00', '2021-03-01 17:00:00', 'u111112', 5);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222223, 1, '2020-08-15 12:00:00', '2021-02-01 20:00:00', 'u111113', 7);
INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, status)
    VALUES (2222224, 1, '2020-08-15 12:00:00', '2021-01-11 11:00:00', 'u111114', 1);

INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333331, 1, '2020-08-15 12:00:00', false, 'Max', 'Kapp', 'max.kapp@foo.ch',
    'uzh.ch', 11111111, null, 2222221);
INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333332, 1, '2020-08-15 12:00:00', false, 'Martin', 'Kappenberger', 'martin.kappenberger@foo.ch',
            'uzh.ch', 2222222, null, 2222222);
INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333333, 1, '2020-08-15 12:00:00', false, 'Martina', 'Kaderli', 'martina.kaderli@foo.ch',
            'uzh.ch', 33333333, null, 2222223);
INSERT INTO o_user(user_id, version, creationdate, informsessiontimeout, u_firstname, u_lastname, u_email,
                   u_institutionalname, u_institutionaluseridentifier, u_employeenumber, fk_identity)
    VALUES (3333334, 1, '2020-08-15 12:00:00', false, 'Max', 'Kappeler', 'max.kappeler@foo.ch',
            'uzh.ch', 44444444, null, 2222224);
