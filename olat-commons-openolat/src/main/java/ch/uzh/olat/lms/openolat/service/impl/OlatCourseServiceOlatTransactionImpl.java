/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.exception.UnknownCourseDesignException;
import ch.uzh.olat.lms.openolat.exception.UnknownCourseTypeException;
import ch.uzh.olat.lms.openolat.model.CourseDesign;
import ch.uzh.olat.lms.openolat.model.CourseType;
import ch.uzh.olat.lms.openolat.service.OlatCourseService;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.olat.core.id.Identity;
import org.olat.core.id.Organisation;
import org.olat.core.id.Roles;
import org.olat.core.util.Formatter;
import org.olat.course.CourseFactory;
import org.olat.course.CourseModule;
import org.olat.course.ICourse;
import org.olat.course.editor.NodeConfigController;
import org.olat.course.nodes.CourseNode;
import org.olat.repository.ErrorList;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryStatusEnum;
import org.olat.repository.RepositoryManager;
import org.olat.repository.RepositoryService;
import org.olat.resource.accesscontrol.ACService;
import org.olat.resource.accesscontrol.Offer;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class OlatCourseServiceOlatTransactionImpl extends OlatCourseServiceTemplate
    implements OlatCourseService {

  private final RepositoryService repositoryService;
  private final RepositoryManager repositoryManager;
  private final CourseModule courseModule;
  private final ACService acService;

  /* Note: Due to several cyclic dependencies of the OpenOLAT beans, it's important that the
   * olat-lms beans don't affect the order in which the OpenOLAT beans are loaded. This can be
   * achieved by Spring's @Lazy annotation, which should always be used when autowiring OpenOLAT
   * services from olat-lms beans.
   */
  public OlatCourseServiceOlatTransactionImpl(
      @Lazy RepositoryService repositoryService,
      @Lazy RepositoryManager repositoryManager,
      @Lazy CourseModule courseModule,
      @Lazy ACService acService) {
    this.repositoryService = repositoryService;
    this.repositoryManager = repositoryManager;
    this.courseModule = courseModule;
    this.acService = acService;
  }

  /**
   * Copy OLAT course (repository entry, olat resource and course nodes). The implementation is
   * inspired by the CoursesWebService#copyCourse method.
   *
   * <p>This method uses OLAT services. Therefore, it must not be annotated by the Spring Data
   * transactional annotation!
   *
   * @param repositoryEntryOrig repository entry of course to be copied
   * @param displayName used for display name of copied repository entry and for long title of root
   *     nodes of course run and course editor
   * @param shortTitle short title of root nodes of course run and course editor; if null
   *     displayName is used instead
   * @param status status of copied repository entry
   * @param managedFlags managed flags of copied repository entry
   * @param organisationsToAssign organisations to be assigned to copied course. If empty or null,
   *     the organisations of the source course are assigned to the copied course.
   * @param organisationsToCreateWithoutBookingOffer organisations for which "Without booking" offer
   *     has to be created. If empty or null, no "Without booking" offer will be created.
   * @param identity user copying course
   * @return repository entry of copied course
   */
  @Override
  public RepositoryEntry copyCourse(
      RepositoryEntry repositoryEntryOrig,
      String displayName,
      String shortTitle,
      RepositoryEntryStatusEnum status,
      String managedFlags,
      List<Organisation> organisationsToAssign,
      List<Organisation> organisationsToCreateWithoutBookingOffer,
      Identity identity) {

    // Copy course
    String displayNameTruncated = Formatter.truncate(displayName, 250);
    RepositoryEntry copiedRepositoryEntry =
        repositoryService.copy(repositoryEntryOrig, identity, displayNameTruncated);

    // Perform some updates for repository entry
    copiedRepositoryEntry.setEntryStatus(status);
    copiedRepositoryEntry.setManagedFlagsString(managedFlags);
    if (organisationsToCreateWithoutBookingOffer != null
        && !organisationsToCreateWithoutBookingOffer.isEmpty()) {
      copiedRepositoryEntry.setPublicVisible(true);
    }
    copiedRepositoryEntry = repositoryService.update(copiedRepositoryEntry);

    // Assign organisations
    if (organisationsToAssign != null && !organisationsToAssign.isEmpty()) {
      copiedRepositoryEntry = assignOrganisations(copiedRepositoryEntry, organisationsToAssign);
    }

    // Create "Without booking" offer
    if (organisationsToCreateWithoutBookingOffer != null
        && !organisationsToCreateWithoutBookingOffer.isEmpty()) {
      createWithoutBookingOffer(copiedRepositoryEntry, organisationsToCreateWithoutBookingOffer);
    }

    // Perform some updates for run model and editor model
    ICourse copiedCourse =
        CourseFactory.openCourseEditSession(
            copiedRepositoryEntry.getOlatResource().getResourceableId());

    CourseNode courseRunRootNode = copiedCourse.getRunStructure().getRootNode();
    CourseNode courseEditorRootNode =
        copiedCourse.getEditorTreeModel().getCourseNode(courseRunRootNode.getIdent());
    String shortTitleTruncated =
        (StringUtils.isNotEmpty(shortTitle))
            ? Formatter.truncate(shortTitle, NodeConfigController.SHORT_TITLE_MAX_LENGTH)
            : Formatter.truncate(displayName, NodeConfigController.SHORT_TITLE_MAX_LENGTH);
    courseRunRootNode.setShortTitle(shortTitleTruncated);
    courseRunRootNode.setLongTitle(displayNameTruncated);
    courseEditorRootNode.setShortTitle(shortTitleTruncated);
    courseEditorRootNode.setLongTitle(displayNameTruncated);

    CourseFactory.saveCourse(copiedCourse.getResourceableId());
    CourseFactory.closeCourseEditSession(copiedCourse.getResourceableId(), true);

    return copiedRepositoryEntry;
  }

  private RepositoryEntry assignOrganisations(
      RepositoryEntry repositoryEntry, List<Organisation> organisationsToAssign) {
    return repositoryManager.setAccess(
        repositoryEntry,
        repositoryEntry.isPublicVisible(),
        repositoryEntry.getAllowToLeaveOption(),
        repositoryEntry.getCanCopy(),
        repositoryEntry.getCanReference(),
        repositoryEntry.getCanDownload(),
        repositoryEntry.getCanIndexMetadata(),
        organisationsToAssign);
  }

  // Implementation inspired by AccessConfigurationController#editOpenAccessOffer and
  // AccessConfigurationController#commitChanges methods
  private void createWithoutBookingOffer(
      RepositoryEntry repositoryEntry, List<Organisation> offerOrganisations) {
    Offer withoutBookingOffer =
        acService.createOffer(repositoryEntry.getOlatResource(), repositoryEntry.getDisplayname());
    withoutBookingOffer.setOpenAccess(true);
    acService.save(withoutBookingOffer);
    acService.updateOfferOrganisations(withoutBookingOffer, offerOrganisations);
  }

  @Override
  public ErrorList permanentlyDeleteCourse(
      RepositoryEntry repositoryEntry, Identity identity, Roles roles, Locale locale) {
    return repositoryService.deletePermanently(repositoryEntry, identity, roles, locale);
  }

  @Override
  public CourseType getCourseType(RepositoryEntry repositoryEntry)
      throws UnknownCourseTypeException {
    return doGetCourseType(repositoryEntry);
  }

  @Override
  public CourseType getCourseTypeFromCourseConfig(RepositoryEntry repositoryEntry)
      throws UnknownCourseTypeException {
    return doGetCourseTypeFromCourseConfig(repositoryEntry);
  }

  @Override
  public CourseDesign getCourseDesign(RepositoryEntry repositoryEntry)
      throws UnknownCourseDesignException {
    return doGetCourseDesign(repositoryEntry);
  }

  @Override
  public CourseDesign getDefaultCourseDesign() {
    String openOlatNameForDefaultCourseDesign = courseModule.getCourseTypeDefault();
    Optional<CourseDesign> defaultCourseDesignOptional =
        CourseDesign.getByOpenOlatName(openOlatNameForDefaultCourseDesign);
    return defaultCourseDesignOptional.orElse(CourseDesign.CLASSIC);
  }

  @Override
  public void changeCourseDesignOfLearningPathCourseTo(
      RepositoryEntry repositoryEntry, CourseDesign newCourseDesign)
      throws UnknownCourseDesignException {
    doChangeCourseDesignOfLearningPathCourseTo(repositoryEntry, newCourseDesign);
  }
}
