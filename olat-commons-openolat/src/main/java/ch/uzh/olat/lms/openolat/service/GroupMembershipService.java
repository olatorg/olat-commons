/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import java.util.Collection;
import java.util.List;
import org.olat.basesecurity.Group;
import org.olat.basesecurity.GroupMembershipInheritance;
import org.olat.basesecurity.GroupRoles;
import org.olat.group.BusinessGroup;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface GroupMembershipService {

  @SuppressWarnings("unused") // used by olat-campuskurs
  List<SimpleIdentity> getIdentitiesOfGroupMembersByBusinessGroupAndGroupRole(
      BusinessGroup businessGroup, GroupRoles groupRole);

  @SuppressWarnings("unused") // used by olat-campuskurs
  List<SimpleIdentity>
      getIdentitiesOfMembersOfRepositoryEntryDefaultGroupByRepositoryEntryIdAndGroupRole(
          long repositoryEntryId, GroupRoles groupRole);

  void addNewMemberToGroup(
      SimpleIdentity identity,
      Group group,
      String role,
      GroupMembershipInheritance groupMembershipInheritance);

  @SuppressWarnings("unused") // used by olat-campuskurs
  void addNewMembersToBusinessGroup(
      Collection<SimpleIdentity> identities, BusinessGroup businessGroup, GroupRoles groupRole);

  @SuppressWarnings("unused") // used by olat-campuskurs
  void addNewMembersToRepositoryEntryDefaultGroup(
      Collection<SimpleIdentity> identities, long repositoryEntryId, GroupRoles groupRole);

  @SuppressWarnings("unused") // used by olat-campuskurs
  void removeMembersFromBusinessGroup(
      Collection<SimpleIdentity> identities, BusinessGroup businessGroup, GroupRoles groupRole);
}
