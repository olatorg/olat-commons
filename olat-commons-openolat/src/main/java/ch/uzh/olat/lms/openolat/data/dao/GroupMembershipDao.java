/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import java.util.List;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.olat.core.commons.persistence.DB;
import org.springframework.stereotype.Component;

/**
 * DAO for DB queries within OLAT transactions.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Component
public class GroupMembershipDao {

  private final DB dbInstance;

  public GroupMembershipDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public List<GroupMembershipImpl> findByGroupKeyAndRoleWithEagerLoading(
      long groupKey, String role) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT membership FROM bgroupmember membership "
                + "LEFT JOIN FETCH membership.group "
                + "LEFT JOIN FETCH membership.identity "
                + "WHERE membership.group.key = :groupKey "
                + "AND membership.role = :role",
            GroupMembershipImpl.class)
        .setParameter("groupKey", groupKey)
        .setParameter("role", role)
        .getResultList();
  }

  public List<GroupMembershipImpl> findByGroupKeyAndIdentityKeyInAndRole(
      long groupKey, List<Long> identityKeys, String role) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT membership FROM bgroupmember membership "
                + "WHERE membership.group.key = :groupKey "
                + "AND membership.identity.key IN (:identityKeys) "
                + "AND membership.role = :role",
            GroupMembershipImpl.class)
        .setParameter("groupKey", groupKey)
        .setParameter("identityKeys", identityKeys)
        .setParameter("role", role)
        .getResultList();
  }

  public void save(GroupMembershipImpl groupMembershipImpl) {
    dbInstance.saveObject(groupMembershipImpl);
  }

  public void delete(GroupMembershipImpl groupMembershipImpl) {
    dbInstance.deleteObject(groupMembershipImpl);
  }
}
