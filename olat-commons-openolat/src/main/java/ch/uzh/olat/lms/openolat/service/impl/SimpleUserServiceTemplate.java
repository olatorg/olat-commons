/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.olat.core.commons.persistence.PersistenceHelper;
import org.olat.core.id.Identity;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public abstract class SimpleUserServiceTemplate {

  protected String doGetUserName(Identity identity) {
    Optional<String> nicknameOptional = getNicknameByIdentityId(identity.getKey());
    return (nicknameOptional.isPresent() && !nicknameOptional.get().isBlank())
        ? nicknameOptional.get()
        : identity.getName();
  }

  protected Map<Long, String> doGetMapOfIdentityIdsAndNotEmptyNicknamesByIdentityIds(
      List<Long> identityIds) {

    // Get list of identity ids and nicknames
    List<IdentityIdAndNickname> identityIdAndNicknames = new ArrayList<>(identityIds.size());
    for (List<Long> chunkOfIdentityIds : PersistenceHelper.collectionOfChunks(identityIds)) {
      List<IdentityIdAndNickname> chunkOfIdentityIdAndNicknames =
          getIdentityIdAndNicknamesByIdentityIdIn(chunkOfIdentityIds);
      identityIdAndNicknames.addAll(chunkOfIdentityIdAndNicknames);
    }

    // Filter by not empty nickname and convert to map of identity ids and nicknames
    return identityIdAndNicknames.stream()
        .filter(
            identityIdAndNickname ->
                identityIdAndNickname.nickname() != null
                    && !identityIdAndNickname.nickname().isBlank())
        .collect(
            Collectors.toMap(IdentityIdAndNickname::identityId, IdentityIdAndNickname::nickname));
  }

  protected abstract Optional<String> getNicknameByIdentityId(long identityId);

  protected abstract List<IdentityIdAndNickname> getIdentityIdAndNicknamesByIdentityIdIn(
      List<Long> identityIds);
}
