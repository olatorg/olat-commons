/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import java.util.Optional;
import org.olat.basesecurity.model.OrganisationImpl;
import org.olat.core.id.Organisation;

/**
 * @author Martin Schraner
 * @since 4.0
 */
public abstract class OlatOrganisationServiceTemplate {

  protected static final String DEFAULT_ORGANISATION_IDENTIFIER = "default-org";
  protected static final String UZH_ORGANISATION_IDENTIFIER = "uzh.ch";

  protected Organisation doGetDefaultOrganisation() {
    return findByIdentifier(DEFAULT_ORGANISATION_IDENTIFIER)
        .orElseThrow(() -> new RuntimeException("No default organisation found!"));
  }

  protected Optional<OrganisationImpl> doGetUzhOrganisation() {
    return findByIdentifier(UZH_ORGANISATION_IDENTIFIER);
  }

  protected abstract Optional<OrganisationImpl> findByIdentifier(String identifier);
}
