/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import java.util.List;
import java.util.Optional;
import org.olat.repository.RepositoryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Repository
public interface RepositoryEntryRepository extends JpaRepository<RepositoryEntry, Long> {

  @Query("SELECT re FROM repositoryentry re LEFT JOIN FETCH re.olatResource WHERE re.key = :id")
  Optional<RepositoryEntry> findByIdWithEagerLoadingOfOlatResource(@Param("id") long id);

  @Query("SELECT re FROM repositoryentry re LEFT JOIN FETCH re.lifecycle WHERE re.key = :id")
  Optional<RepositoryEntry> findByIdWithEagerLoadingOfRepositoryEntryLifecycle(
      @Param("id") long id);

  @SuppressWarnings("JpaQlInspection")
  @Query(
      "SELECT DISTINCT re FROM repositoryentry re "
          + "JOIN re.groups retogroup "
          + "ON retogroup.defaultGroup = true "
          + "JOIN retogroup.group bsgroup "
          + "JOIN bsgroup.members membership "
          + "ON membership.role = :role "
          + "WHERE membership.identity.key = :identityId "
          + "AND re.technicalType = :technicalType "
          + "AND re.olatResource.resName = :resName "
          + "AND re.status IN (:statusList)")
  List<RepositoryEntry> findByResNameAndTechnicalTypeAndStatusInAndRoleAndIdentityId(
      @Param("resName") String resName,
      @Param("technicalType") String technicalType,
      @Param("statusList") List<String> statusList,
      @Param("role") String role,
      @Param("identityId") long identityId);

  @SuppressWarnings("JpaQlInspection")
  @Query(
      "SELECT DISTINCT re.key FROM repositoryentry re "
          + "JOIN re.groups retogroup "
          + "ON retogroup.defaultGroup = true "
          + "JOIN retogroup.group bsgroup "
          + "JOIN bsgroup.members membership "
          + "ON membership.role = :role "
          + "WHERE membership.identity.key = :identityId "
          + "AND re.technicalType IN (:technicalTypes) "
          + "AND re.olatResource.resName = :resName "
          + "AND re.status IN (:statusList)")
  List<Long> findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
      @Param("resName") String resName,
      @Param("technicalTypes") List<String> technicalTypes,
      @Param("statusList") List<String> statusList,
      @Param("role") String role,
      @Param("identityId") long identityId);

  @SuppressWarnings("JpaQlInspection")
  @Query(
      "SELECT DISTINCT re FROM repositoryentry re "
          + "JOIN re.groups retogroup "
          + "ON retogroup.defaultGroup = true "
          + "JOIN retogroup.group bsgroup "
          + "JOIN bsgroup.members membership "
          + "ON membership.role = :role "
          + "WHERE membership.identity.key = :identityId "
          + "AND re.technicalType IS NULL "
          + "AND re.olatResource.resName = :resName "
          + "AND re.status IN (:statusList)")
  List<RepositoryEntry> findByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
      @Param("resName") String resName,
      @Param("statusList") List<String> statusList,
      @Param("role") String role,
      @Param("identityId") long identityId);
}
