/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import java.util.List;
import java.util.Optional;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Repository
public interface GroupMembershipRepository extends JpaRepository<GroupMembershipImpl, Long> {

  @Query(
      "SELECT membership FROM bgroupmember membership "
          + "LEFT JOIN FETCH membership.group "
          + "LEFT JOIN FETCH membership.identity "
          + "WHERE membership.group.key = :groupKey "
          + "AND membership.role = :role")
  List<GroupMembershipImpl> findByGroupKeyAndRoleWithEagerLoading(
      @Param("groupKey") long groupKey, @Param("role") String role);

  @SuppressWarnings("JpaQlInspection")
  @Query(
      "SELECT membership FROM bgroupmember membership "
          + "WHERE membership.group.key = :groupKey "
          + "AND membership.identity.key = :identityKey "
          + "AND membership.role = :role")
  Optional<GroupMembershipImpl> findByGroupKeyAndIdentityKeyAndRole(
      @Param("groupKey") long groupKey,
      @Param("identityKey") long identityKey,
      @Param("role") String role);

  @SuppressWarnings("JpaQlInspection")
  @Query(
      "SELECT membership FROM bgroupmember membership "
          + "WHERE membership.group.key = :groupKey "
          + "AND membership.identity.key IN (:identityKeys) "
          + "AND membership.role = :role")
  List<GroupMembershipImpl> findByGroupKeyAndIdentityKeyInAndRole(
      @Param("groupKey") long groupKey,
      @Param("identityKeys") List<Long> identityKeys,
      @Param("role") String role);
}
