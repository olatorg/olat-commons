/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.exception.UnknownCourseDesignException;
import ch.uzh.olat.lms.openolat.exception.UnknownCourseTypeException;
import ch.uzh.olat.lms.openolat.model.CourseDesign;
import ch.uzh.olat.lms.openolat.model.CourseType;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.logging.AssertException;
import org.olat.course.CorruptedCourseException;
import org.olat.course.CourseFactory;
import org.olat.course.ICourse;
import org.olat.course.config.CourseConfig;
import org.olat.course.nodes.CourseNode;
import org.olat.course.nodes.st.assessment.STLearningPathConfigs;
import org.olat.course.tree.CourseEditorTreeModel;
import org.olat.modules.ModuleConfiguration;
import org.olat.repository.RepositoryEntry;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Slf4j
public abstract class OlatCourseServiceTemplate {

  private record EditableCourse(
      ICourse course,
      CourseConfig courseConfig,
      CourseNode courseRunRootNode,
      CourseEditorTreeModel courseEditorTreeModel) {}

  protected CourseType doGetCourseType(RepositoryEntry repositoryEntry)
      throws UnknownCourseTypeException {

    // Try to get course type from technical type attribute of repository entry
    Optional<CourseType> courseTypeOptional =
        getCourseTypeFromTechnicalTypeAttributeOfRepositoryEntry(repositoryEntry);
    if (courseTypeOptional.isPresent()) {
      return courseTypeOptional.get();
    }

    // Unfortunately, the technical type attribute of the repository entry is nullable. If the
    // attribute is null, we have to try to get the course type from the course config, which
    // is much more expensive.
    return doGetCourseTypeFromCourseConfig(repositoryEntry);
  }

  private Optional<CourseType> getCourseTypeFromTechnicalTypeAttributeOfRepositoryEntry(
      RepositoryEntry repositoryEntry) {

    if (repositoryEntry.getTechnicalType() == null) {
      log.warn(
          "Cannot determine course design of course '{}' (repository entry id {}) from technical "
              + "type attribute: attribute is null",
          repositoryEntry.getDisplayname(),
          repositoryEntry.getKey());
      return Optional.empty();
    }

    Optional<CourseType> courseTypeOptional =
        CourseType.ofTechnicalType(repositoryEntry.getTechnicalType());
    if (courseTypeOptional.isEmpty()) {
      log.warn(
          "Cannot determine course design of course '{}' (repository entry id {}) from technical "
              + "type attribute: unknown type: {}",
          repositoryEntry.getDisplayname(),
          repositoryEntry.getKey(),
          repositoryEntry.getTechnicalType());
    }
    return courseTypeOptional;
  }

  protected CourseType doGetCourseTypeFromCourseConfig(RepositoryEntry repositoryEntry)
      throws UnknownCourseTypeException {

    // Inspired by method {@link org.olat.course.nodeaccess.ui.NodeAccessSettingsController}
    // to decide if migrateLink should be displayed or not.
    CourseConfig courseConfig;
    String type;
    try {
      ICourse course =
          CourseFactory.loadCourse(repositoryEntry.getOlatResource().getResourceableId());
      courseConfig = course.getCourseConfig();
      type = courseConfig.getNodeAccessType().getType();
    } catch (CorruptedCourseException e) {
      throw new UnknownCourseTypeException(
          getCannotDetermineCourseDesignFromCourseConfigMessage(repositoryEntry)
              + ": course is corrupted");
    }

    if (type == null || type.isBlank()) {
      throw new UnknownCourseTypeException(
          getCannotDetermineCourseDesignFromCourseConfigMessage(repositoryEntry)
              + ": type is null or empty");
    }

    Optional<CourseType> courseTypeOptional = CourseType.ofTechnicalType(type);
    return courseTypeOptional.orElseThrow(
        () ->
            new UnknownCourseTypeException(
                getCannotDetermineCourseDesignFromCourseConfigMessage(repositoryEntry)
                    + ": unknown type: "
                    + type));
  }

  private static String getCannotDetermineCourseDesignFromCourseConfigMessage(
      RepositoryEntry repositoryEntry) {
    return "Cannot determine course design of course '"
        + repositoryEntry.getDisplayname()
        + "' (repository entry id "
        + repositoryEntry.getKey()
        + ") from course config";
  }

  protected CourseDesign doGetCourseDesign(RepositoryEntry repositoryEntry)
      throws UnknownCourseDesignException {

    CourseType courseType;
    try {
      courseType = doGetCourseType(repositoryEntry);
    } catch (UnknownCourseTypeException e) {
      throw new UnknownCourseDesignException(
          "Error when trying to determine course type related to course design: " + e.getMessage());
    }

    if (courseType == CourseType.CONVENTIONAL) {
      return CourseDesign.CLASSIC;
    }

    boolean learningPathSetToSequential;
    CourseConfig courseConfig;
    try {
      ICourse course =
          CourseFactory.loadCourse(repositoryEntry.getOlatResource().getResourceableId());
      learningPathSetToSequential = isLearningPathSetToSequential(course);
      courseConfig = course.getCourseConfig();
    } catch (CorruptedCourseException e) {
      throw new UnknownCourseDesignException(
          getCannotDetermineCourseDesignFromCourseConfigMessage(repositoryEntry)
              + ": course is corrupted");
    }

    if (learningPathSetToSequential
        && courseConfig.isMenuPathEnabled()
        && !courseConfig.isMenuNodeIconsEnabled()) {
      return CourseDesign.WITH_LEARNING_PATH;
    } else if (!learningPathSetToSequential
        && !courseConfig.isMenuPathEnabled()
        && courseConfig.isMenuNodeIconsEnabled()) {
      return CourseDesign.WITH_LEARNING_PROGRESS;
    } else {
      return CourseDesign.USER_DEFINED_LEARNING_PATH_COURSE_DESIGN;
    }
  }

  private static boolean isLearningPathSetToSequential(ICourse course) {
    CourseNode courseRunRootNode = course.getRunStructure().getRootNode();
    ModuleConfiguration moduleConfiguration = courseRunRootNode.getModuleConfiguration();
    String lpSequenceValue =
        moduleConfiguration.getStringValue(STLearningPathConfigs.CONFIG_LP_SEQUENCE_KEY);
    return STLearningPathConfigs.CONFIG_LP_SEQUENCE_VALUE_SEQUENTIAL.equals(lpSequenceValue);
  }

  protected void doChangeCourseDesignOfLearningPathCourseTo(
      RepositoryEntry repositoryEntry, CourseDesign newCourseDesign)
      throws UnknownCourseDesignException {

    if (newCourseDesign != CourseDesign.WITH_LEARNING_PATH
        && newCourseDesign != CourseDesign.WITH_LEARNING_PROGRESS) {
      throw new UnknownCourseDesignException(
          "Course design " + newCourseDesign + " not supported!");
    }

    CourseType courseType;
    try {
      courseType = doGetCourseType(repositoryEntry);
    } catch (UnknownCourseTypeException e) {
      throw new UnknownCourseDesignException(
          "Error when trying to determine course type related to course design: " + e.getMessage());
    }

    if (courseType != CourseType.LEARNING_PATH) {
      throw new AssertException(
          "Course '"
              + repositoryEntry.getDisplayname()
              + "' (repository entry id "
              + repositoryEntry.getKey()
              + ") is not a learning path course!");
    }

    EditableCourse editableCourse = openCourseEditSession(repositoryEntry);
    updateNavigationElementsOfCourseConfig(editableCourse.courseConfig(), newCourseDesign);
    updateLearningPathSequenceElementOfRunStructureAndEditorTreeModelOfRootNode(
        editableCourse.courseRunRootNode(),
        editableCourse.courseEditorTreeModel(),
        newCourseDesign);
    saveChangesAndCloseEditSession(editableCourse);
  }

  private static EditableCourse openCourseEditSession(RepositoryEntry campusCourseRepositoryEntry) {
    ICourse course =
        CourseFactory.openCourseEditSession(
            campusCourseRepositoryEntry.getOlatResource().getResourceableId());
    CourseNode courseRunRootNode = course.getRunStructure().getRootNode();
    CourseEditorTreeModel courseEditorTreeModel = course.getEditorTreeModel();
    return new EditableCourse(
        course, course.getCourseConfig(), courseRunRootNode, courseEditorTreeModel);
  }

  private static void updateNavigationElementsOfCourseConfig(
      CourseConfig courseConfig, CourseDesign newCourseDesign) {
    courseConfig.setMenuEnabled(true);
    courseConfig.setBreadCrumbEnabled(true);
    if (newCourseDesign == CourseDesign.WITH_LEARNING_PATH) {
      courseConfig.setMenuPathEnabled(true);
      courseConfig.setMenuNodeIconsEnabled(false);
    } else if (newCourseDesign == CourseDesign.WITH_LEARNING_PROGRESS) {
      courseConfig.setMenuPathEnabled(false);
      courseConfig.setMenuNodeIconsEnabled(true);
    }
  }

  private static void updateLearningPathSequenceElementOfRunStructureAndEditorTreeModelOfRootNode(
      CourseNode courseRunRootNode,
      CourseEditorTreeModel courseEditorTreeModel,
      CourseDesign newCourseDesign) {
    updateLearningPathSequenceElementOfModuleConfiguration(
        courseRunRootNode.getModuleConfiguration(), newCourseDesign);
    CourseNode courseEditorRootNode =
        courseEditorTreeModel.getCourseNode(courseRunRootNode.getIdent());
    updateLearningPathSequenceElementOfModuleConfiguration(
        courseEditorRootNode.getModuleConfiguration(), newCourseDesign);
  }

  private static void updateLearningPathSequenceElementOfModuleConfiguration(
      ModuleConfiguration moduleConfiguration, CourseDesign newCourseDesign) {

    if (!moduleConfiguration.has(STLearningPathConfigs.CONFIG_LP_SEQUENCE_KEY)) {
      log.error(
          "Cannot update learningPathSequence element of module configuration: key "
              + STLearningPathConfigs.CONFIG_LP_SEQUENCE_KEY
              + " does not exist!");
      return;
    }

    if (newCourseDesign == CourseDesign.WITH_LEARNING_PATH) {
      moduleConfiguration.setStringValue(
          STLearningPathConfigs.CONFIG_LP_SEQUENCE_KEY,
          STLearningPathConfigs.CONFIG_LP_SEQUENCE_VALUE_SEQUENTIAL);
    } else if (newCourseDesign == CourseDesign.WITH_LEARNING_PROGRESS) {
      moduleConfiguration.setStringValue(
          STLearningPathConfigs.CONFIG_LP_SEQUENCE_KEY,
          STLearningPathConfigs.CONFIG_LP_SEQUENCE_VALUE_WITHOUT);
    }
  }

  private static void saveChangesAndCloseEditSession(EditableCourse editableCourse) {
    CourseFactory.setCourseConfig(
        editableCourse.course().getResourceableId(), editableCourse.courseConfig());
    CourseFactory.saveCourse(editableCourse.course().getResourceableId());
    editableCourse.courseEditorTreeModel().nodeConfigChanged(editableCourse.courseRunRootNode());
    CourseFactory.saveCourseEditorTreeModel(editableCourse.course().getResourceableId());
    CourseFactory.closeCourseEditSession(editableCourse.course().getResourceableId(), true);
  }
}
