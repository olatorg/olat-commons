/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import jakarta.persistence.NoResultException;
import java.util.Optional;
import org.olat.basesecurity.model.OrganisationImpl;
import org.olat.core.commons.persistence.DB;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 * DAO for DB queries within OLAT transactions.
 *
 * @author Martin Schraner
 * @since 4.0
 */
@Component("olatOrganisationDao")
public class OrganisationDao {

  private final DB dbInstance;

  public OrganisationDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public Optional<OrganisationImpl> findByIdentifier(String identifier) {
    try {
      return Optional.of(
          dbInstance
              .getCurrentEntityManager()
              .createQuery(
                  "SELECT org FROM organisation org WHERE org.identifier = :identifier",
                  OrganisationImpl.class)
              .setParameter("identifier", identifier)
              .getSingleResult());
    } catch (NoResultException | EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }
}
