/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.data.repository.GroupMembershipRepository;
import ch.uzh.olat.lms.openolat.data.repository.OrganisationRepository;
import ch.uzh.olat.lms.openolat.service.GroupMembershipService;
import ch.uzh.olat.lms.openolat.service.OlatOrganisationService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.olat.basesecurity.GroupMembershipInheritance;
import org.olat.basesecurity.OrganisationRoles;
import org.olat.basesecurity.OrganisationStatus;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.olat.basesecurity.model.OrganisationImpl;
import org.olat.core.id.Organisation;
import org.olat.core.logging.AssertException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class contains methods from the OpenOLAT OrganisationService class adapted to be used with
 * Spring data (i.e. without DbImpl).
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class OlatOrganisationServiceSpringTransactionImpl extends OlatOrganisationServiceTemplate
    implements OlatOrganisationService {

  private final GroupMembershipService groupMembershipService;
  private final OrganisationRepository organisationRepository;
  private final GroupMembershipRepository groupMembershipRepository;

  public OlatOrganisationServiceSpringTransactionImpl(
      @Qualifier("groupMembershipServiceSpringTransactionImpl")
          GroupMembershipService groupMembershipService,
      OrganisationRepository organisationRepository,
      GroupMembershipRepository groupMembershipRepository) {
    this.groupMembershipService = groupMembershipService;
    this.organisationRepository = organisationRepository;
    this.groupMembershipRepository = groupMembershipRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Organisation getDefaultOrganisation() {
    return doGetDefaultOrganisation();
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<OrganisationImpl> getUzhOrganisation() {
    return doGetUzhOrganisation();
  }

  @Override
  @Transactional
  public void addNewMemberToOrganisationIfNotExistingYet(
      SimpleIdentity identity, Organisation organisation, OrganisationRoles organisationRole) {

    GroupMembershipInheritance inheritanceMode;
    if (OrganisationRoles.isInheritedByDefault(organisationRole)) {
      inheritanceMode = GroupMembershipInheritance.root;
    } else {
      inheritanceMode = GroupMembershipInheritance.none;
    }

    createOrUpdateGroupMembership(identity, organisation, organisationRole, inheritanceMode);
  }

  private void createOrUpdateGroupMembership(
      SimpleIdentity identity,
      Organisation organisation,
      OrganisationRoles organisationRole,
      GroupMembershipInheritance inheritanceMode) {

    if (inheritanceMode == GroupMembershipInheritance.inherited) {
      throw new AssertException("Inherited are automatic");
    }

    createOrUpdateGroupMembershipForOrganisation(
        identity, organisation, organisationRole, inheritanceMode);

    if (inheritanceMode == GroupMembershipInheritance.root) {
      createOrUpdateGroupMembershipForDescendantOrganisations(
          identity, organisation, organisationRole);
    }
  }

  private void createOrUpdateGroupMembershipForOrganisation(
      SimpleIdentity identity,
      Organisation organisation,
      OrganisationRoles organisationRole,
      GroupMembershipInheritance inheritanceMode) {

    Optional<GroupMembershipImpl> groupMembershipOptional =
        groupMembershipRepository.findByGroupKeyAndIdentityKeyAndRole(
            organisation.getGroup().getKey(), identity.getId(), organisationRole.name());

    if (groupMembershipOptional.isEmpty()) {
      groupMembershipService.addNewMemberToGroup(
          identity, organisation.getGroup(), organisationRole.name(), inheritanceMode);
    } else if (inheritanceMode != groupMembershipOptional.get().getInheritanceMode()) {
      groupMembershipOptional.get().setInheritanceMode(inheritanceMode);
      groupMembershipOptional.get().setLastModified(new Date());
    }
  }

  private void createOrUpdateGroupMembershipForDescendantOrganisations(
      SimpleIdentity identity, Organisation organisation, OrganisationRoles organisationRole) {

    List<OrganisationImpl> descendantOrganisations = getDescendantOrganisations(organisation);

    for (OrganisationImpl descendantOrganisation : descendantOrganisations) {
      Optional<GroupMembershipImpl> inheritedMembershipOptional =
          groupMembershipRepository.findByGroupKeyAndIdentityKeyAndRole(
              descendantOrganisation.getGroup().getKey(),
              identity.getId(),
              organisationRole.name());

      if (inheritedMembershipOptional.isEmpty()) {
        groupMembershipService.addNewMemberToGroup(
            identity,
            descendantOrganisation.getGroup(),
            organisationRole.name(),
            GroupMembershipInheritance.inherited);
      } else if (inheritedMembershipOptional.get().getInheritanceMode()
          == GroupMembershipInheritance.none) {
        inheritedMembershipOptional.get().setInheritanceMode(GroupMembershipInheritance.inherited);
        inheritedMembershipOptional.get().setLastModified(new Date());
      }
    }
  }

  private List<OrganisationImpl> getDescendantOrganisations(Organisation organisation) {
    List<String> notDeletedStatuses = new ArrayList<>();
    for (OrganisationStatus organisationStatus : OrganisationStatus.notDelete()) {
      notDeletedStatuses.add(organisationStatus.name());
    }
    return organisationRepository.findDescendants(
        organisation.getKey(), organisation.getMaterializedPathKeys(), notDeletedStatuses);
  }

  @Override
  protected Optional<OrganisationImpl> findByIdentifier(String identifier) {
    return organisationRepository.findByIdentifier(identifier);
  }
}
