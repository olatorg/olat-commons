/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.model;

import java.util.Optional;
import lombok.Getter;
import org.olat.course.condition.ConditionNodeAccessProvider;
import org.olat.course.learningpath.manager.LearningPathNodeAccessProvider;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Getter
public enum CourseType {
  LEARNING_PATH(LearningPathNodeAccessProvider.TYPE),
  CONVENTIONAL(ConditionNodeAccessProvider.TYPE);

  private final String technicalType;

  CourseType(String technicalType) {
    this.technicalType = technicalType;
  }

  public static Optional<CourseType> ofTechnicalType(String technicalType) {
    for (CourseType courseType : values()) {
      if (courseType.getTechnicalType().equalsIgnoreCase(technicalType)) {
        return Optional.of(courseType);
      }
    }
    return Optional.empty();
  }
}
