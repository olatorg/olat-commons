/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.data.repository.GroupMembershipRepository;
import ch.uzh.olat.lms.openolat.data.repository.RepositoryEntryToGroupRelationRepository;
import ch.uzh.olat.lms.openolat.service.GroupMembershipService;
import java.util.Collection;
import java.util.List;
import org.olat.basesecurity.Group;
import org.olat.basesecurity.GroupMembershipInheritance;
import org.olat.basesecurity.GroupRoles;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.olat.group.BusinessGroup;
import org.olat.repository.model.RepositoryEntryToGroupRelation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class GroupMembershipServiceSpringTransactionImpl extends GroupMembershipServiceTemplate
    implements GroupMembershipService {

  private final GroupMembershipRepository groupMembershipRepository;
  private final RepositoryEntryToGroupRelationRepository repositoryEntryToGroupRelationRepository;

  public GroupMembershipServiceSpringTransactionImpl(
      GroupMembershipRepository groupMembershipRepository,
      RepositoryEntryToGroupRelationRepository repositoryEntryToGroupRelationRepository) {
    this.groupMembershipRepository = groupMembershipRepository;
    this.repositoryEntryToGroupRelationRepository = repositoryEntryToGroupRelationRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public List<SimpleIdentity> getIdentitiesOfGroupMembersByBusinessGroupAndGroupRole(
      BusinessGroup businessGroup, GroupRoles groupRole) {
    return doGetIdentitiesOfGroupMembersByBusinessGroupAndGroupRole(businessGroup, groupRole);
  }

  @Override
  @Transactional(readOnly = true)
  public List<SimpleIdentity>
      getIdentitiesOfMembersOfRepositoryEntryDefaultGroupByRepositoryEntryIdAndGroupRole(
          long repositoryEntryId, GroupRoles groupRole) {
    return doGetIdentitiesOfMembersOfRepositoryEntryDefaultGroupByRepositoryEntryIdAndGroupRole(
        repositoryEntryId, groupRole);
  }

  @Override
  @Transactional
  public void addNewMemberToGroup(
      SimpleIdentity simpleIdentity,
      Group group,
      String role,
      GroupMembershipInheritance groupMembershipInheritance) {
    doAddNewMemberToGroup(simpleIdentity, group, role, groupMembershipInheritance);
  }

  @Override
  @Transactional
  public void addNewMembersToBusinessGroup(
      Collection<SimpleIdentity> simpleIdentities,
      BusinessGroup businessGroup,
      GroupRoles groupRole) {
    doAddNewMembersToBusinessGroup(simpleIdentities, businessGroup, groupRole);
  }

  @Override
  @Transactional
  public void addNewMembersToRepositoryEntryDefaultGroup(
      Collection<SimpleIdentity> simpleIdentities, long repositoryEntryId, GroupRoles groupRole) {
    doAddNewMembersToRepositoryEntryDefaultGroup(simpleIdentities, repositoryEntryId, groupRole);
  }

  @Override
  @Transactional
  public void removeMembersFromBusinessGroup(
      Collection<SimpleIdentity> simpleIdentities,
      BusinessGroup businessGroup,
      GroupRoles groupRole) {
    doRemoveMembersFromBusinessGroup(simpleIdentities, businessGroup, groupRole);
  }

  @Override
  protected List<GroupMembershipImpl> getGroupMembershipsByGroupKeyAndRole(
      long groupKey, String roleName) {
    return groupMembershipRepository.findByGroupKeyAndRoleWithEagerLoading(groupKey, roleName);
  }

  @Override
  protected List<GroupMembershipImpl> getGroupMembershipsByGroupKeyAndIdentityKeysAndRole(
      Long groupKey, List<Long> identityKeys, String roleName) {
    return groupMembershipRepository.findByGroupKeyAndIdentityKeyInAndRole(
        groupKey, identityKeys, roleName);
  }

  @Override
  protected void saveGroupMembership(GroupMembershipImpl groupMembershipImpl) {
    groupMembershipRepository.save(groupMembershipImpl);
  }

  @Override
  protected void deleteGroupMembership(GroupMembershipImpl groupMembershipImpl) {
    groupMembershipRepository.delete(groupMembershipImpl);
  }

  @Override
  protected List<RepositoryEntryToGroupRelation>
      getRepositoryEntryToGroupRelationsOfDefaultGroupByRepositoryEntryId(long repositoryEntryId) {
    return repositoryEntryToGroupRelationRepository.findByRepositoryEntryIdAndDefaultGroup(
        repositoryEntryId, true);
  }
}
