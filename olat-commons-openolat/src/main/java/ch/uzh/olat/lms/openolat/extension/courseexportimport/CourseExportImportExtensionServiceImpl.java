/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.extension.courseexportimport;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.logging.OLATRuntimeException;
import org.olat.core.util.ZipUtil;
import org.olat.core.util.xml.XStreamHelper;
import org.olat.course.ICourse;
import org.olat.course.PersistingCourseImpl;
import org.olat.course.assessment.AssessmentMode;
import org.olat.course.assessment.AssessmentModeManager;
import org.olat.course.assessment.model.AssessmentModeImpl;
import org.olat.course.groupsandrights.CourseGroupManager;
import org.olat.group.BusinessGroup;
import org.olat.group.area.BGArea;
import org.olat.repository.RepositoryEntry;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
@Slf4j
public class CourseExportImportExtensionServiceImpl implements CourseExportImportExtensionService {

  private static final String ASSESSMENT_MODES_FILENAME = "AssessmentModes.xml";
  public static final String ASSESSMENT_MODES_DETAILS_FILENAME = "AssessmentModesDetails.xml";

  private final AssessmentModeManager assessmentModeManager;
  private final AssessmentModeExportImportListener[] assessmentModeExportImportListeners;

  public CourseExportImportExtensionServiceImpl(
      AssessmentModeManager assessmentModeManager,
      AssessmentModeExportImportListener[] assessmentModeExportImportListeners) {
    this.assessmentModeManager = assessmentModeManager;
    this.assessmentModeExportImportListeners = assessmentModeExportImportListeners;
  }

  @Override
  public void exportAssessmentModes(
      PersistingCourseImpl sourceCourse, ZipOutputStream zipOutputStream) {

    // Get assessment modes
    RepositoryEntry repositoryEntry =
        sourceCourse.getCourseEnvironment().getCourseGroupManager().getCourseEntry();
    List<AssessmentMode> assessmentModes =
        assessmentModeManager.getAssessmentModeFor(repositoryEntry);

    // Don't create export files if there is nothing to export
    if (assessmentModes.isEmpty()) {
      return;
    }

    // Export assessment modes
    try {
      exportAssessmentModesWithoutGroupsAndAreasToXmlFile(assessmentModes, zipOutputStream);
      exportAssessmentModesGroupsAndAreasToXmlFile(assessmentModes, zipOutputStream);
    } catch (IOException e) {
      log.error(
          "Error when trying to export assessment modes of course '{}': ",
          repositoryEntry.getDisplayname(),
          e);
      throw new OLATRuntimeException(e.getMessage());
    }

    // Inform listeners
    for (AssessmentModeExportImportListener assessmentModeExportImportListener :
        assessmentModeExportImportListeners) {
      assessmentModeExportImportListener.onAfterExport(
          assessmentModes, repositoryEntry, zipOutputStream);
    }
  }

  @SuppressWarnings("java:S6204")
  private void exportAssessmentModesWithoutGroupsAndAreasToXmlFile(
      List<AssessmentMode> assessmentModes, ZipOutputStream zipOutputStream) throws IOException {

    List<AssessmentModeWithoutGroupsAndAreasExportImport>
        assessmentModesWithoutGroupsAndAreasExportImportList =
            assessmentModes.stream()
                .map(AssessmentModeImpl.class::cast)
                .map(AssessmentModeWithoutGroupsAndAreasExportImport::of)
                .collect(Collectors.toList());

    zipOutputStream.putNextEntry(
        new ZipEntry(ZipUtil.concat(ICourse.EXPORTED_DATA_FOLDERNAME, ASSESSMENT_MODES_FILENAME)));
    XStream xStream = XStreamHelper.createXStreamInstance();
    xStream.toXML(assessmentModesWithoutGroupsAndAreasExportImportList, zipOutputStream);
    zipOutputStream.closeEntry();
  }

  @SuppressWarnings("java:S6204")
  private void exportAssessmentModesGroupsAndAreasToXmlFile(
      List<AssessmentMode> assessmentModes, ZipOutputStream zipOutputStream) throws IOException {

    List<AssessmentModeGroupsAndAreasExportImport> assessmentModeGroupsAndAreasExportImportList =
        assessmentModes.stream()
            .map(AssessmentModeGroupsAndAreasExportImport::of)
            .collect(Collectors.toList());

    zipOutputStream.putNextEntry(
        new ZipEntry(
            ZipUtil.concat(ICourse.EXPORTED_DATA_FOLDERNAME, ASSESSMENT_MODES_DETAILS_FILENAME)));
    XStream xStream = XStreamHelper.createXStreamInstance();
    xStream.toXML(assessmentModeGroupsAndAreasExportImportList, zipOutputStream);
    zipOutputStream.closeEntry();
  }

  @Override
  public void importAssessmentModes(
      RepositoryEntry importedRepositoryEntry,
      CourseGroupManager courseGroupManager,
      File fImportBaseDirectory) {

    try {
      // Import assessment modes without courses and groups
      List<AssessmentModeWithoutGroupsAndAreasExportImport>
          assessmentModesWithoutGroupsAndAreasExportImportList =
              importAssessmentModesWithoutGroupsAndAreasFromXmlFile(fImportBaseDirectory);

      // Import assessment mode courses and groups
      List<AssessmentModeGroupsAndAreasExportImport> assessmentModeGroupsAndAreasExportImportList =
          importAssessmentModeGroupsAndAreasFromXmlFile(fImportBaseDirectory);

      // Save assessment modes without courses and groups
      Map<Long, AssessmentMode> mapOfOriginalAssessmentModeKeyAndSavedAssessmentModes =
          saveAssessmentModesWithoutGroupsAndAreasToDB(
              assessmentModesWithoutGroupsAndAreasExportImportList, importedRepositoryEntry);

      // Save assessment mode groups and areas
      saveAssessmentModesGroupsAndAreasToDB(
          assessmentModeGroupsAndAreasExportImportList,
          mapOfOriginalAssessmentModeKeyAndSavedAssessmentModes,
          importedRepositoryEntry,
          courseGroupManager);

      // Inform listeners
      for (AssessmentModeExportImportListener assessmentModeImportListener :
          assessmentModeExportImportListeners) {
        assessmentModeImportListener.onAfterImport(
            mapOfOriginalAssessmentModeKeyAndSavedAssessmentModes,
            importedRepositoryEntry,
            fImportBaseDirectory);
      }

    } catch (Exception e) {
      throw new AssessmentModeImportException(e, importedRepositoryEntry);
    }
  }

  @SuppressWarnings("unchecked")
  private List<AssessmentModeWithoutGroupsAndAreasExportImport>
      importAssessmentModesWithoutGroupsAndAreasFromXmlFile(File fImportBaseDirectory)
          throws IOException {

    File assessmentModesFile = new File(fImportBaseDirectory, ASSESSMENT_MODES_FILENAME);
    if (!assessmentModesFile.exists()) {
      return new ArrayList<>();
    }

    try (InputStream fileInputStream = new FileInputStream(assessmentModesFile)) {
      XStream xStream = XStreamHelper.createXStreamInstance();
      XStreamHelper.allowDefaultPackage(xStream);
      xStream.allowTypesByWildcard(
          new String[] {"ch.uzh.olat.lms.openolat.extension.courseexportimport.**"});
      xStream.ignoreUnknownElements(); // Provide backwards compatibility
      // Define aliases to provide backwards compatibility
      xStream.aliasType(
          "org.olat.course.assessment.model.AssessmentModeImpl",
          AssessmentModeWithoutGroupsAndAreasExportImport.class);
      return (List<AssessmentModeWithoutGroupsAndAreasExportImport>)
          xStream.fromXML(fileInputStream);
    }
  }

  @SuppressWarnings("unchecked")
  private List<AssessmentModeGroupsAndAreasExportImport>
      importAssessmentModeGroupsAndAreasFromXmlFile(File fImportBaseDirectory) throws IOException {

    File assessmentModesDetailsFile =
        new File(fImportBaseDirectory, ASSESSMENT_MODES_DETAILS_FILENAME);
    if (!assessmentModesDetailsFile.exists()) {
      return new ArrayList<>();
    }

    try (InputStream fileInputStream = new FileInputStream(assessmentModesDetailsFile)) {
      XStream xStream = XStreamHelper.createXStreamInstance();
      XStreamHelper.allowDefaultPackage(xStream);
      xStream.allowTypesByWildcard(
          new String[] {"ch.uzh.olat.lms.openolat.extension.courseexportimport.**"});
      // Define aliases to provide backwards compatibility
      xStream.aliasType(
          "org.olat.course.assessment.AssessmentModeDetailsImportExport",
          AssessmentModeGroupsAndAreasExportImport.class);
      xStream.aliasType(
          "org.olat.course.assessment.AssessmentModeDetailsImportExport$Group",
          AssessmentModeGroupsAndAreasExportImport.Group.class);
      xStream.aliasType(
          "org.olat.course.assessment.AssessmentModeDetailsImportExport$Area",
          AssessmentModeGroupsAndAreasExportImport.Area.class);
      xStream.aliasType(
          "ch.uzh.olat.lms.sebserver.courseexportimport.SebServerAssessmentModeDetailsImportExport",
          AssessmentModeGroupsAndAreasExportImport.class);
      return (List<AssessmentModeGroupsAndAreasExportImport>) xStream.fromXML(fileInputStream);
    }
  }

  private Map<Long, AssessmentMode> saveAssessmentModesWithoutGroupsAndAreasToDB(
      List<AssessmentModeWithoutGroupsAndAreasExportImport>
          assessmentModesWithoutGroupsAndAreasExportImportList,
      RepositoryEntry importedRepositoryEntry) {

    Map<Long, AssessmentMode> mapOfOriginalAssessmentModeKeysAndImportedAndSavedAssessmentModes =
        new HashMap<>();

    for (AssessmentModeWithoutGroupsAndAreasExportImport
        assessmentModeWithoutGroupsAndAreasExportImport :
            assessmentModesWithoutGroupsAndAreasExportImportList) {

      // Convert to AssessmentModeImpl
      AssessmentModeImpl assessmentModeImpl =
          assessmentModeWithoutGroupsAndAreasExportImport.toAssessmentModeImpl();

      // Set id to null (has to get recreated when saving to DB), set creation date / last modified,
      // set repository entry to importedRepositoryEntry
      assessmentModeImpl.setKey(null);
      assessmentModeImpl.setCreationDate(new Date());
      assessmentModeImpl.setLastModified(assessmentModeImpl.getCreationDate());
      assessmentModeImpl.setRepositoryEntry(importedRepositoryEntry);

      // Save to DB
      assessmentModeImpl = (AssessmentModeImpl) assessmentModeManager.persist(assessmentModeImpl);

      mapOfOriginalAssessmentModeKeysAndImportedAndSavedAssessmentModes.put(
          assessmentModeWithoutGroupsAndAreasExportImport.getKey(), assessmentModeImpl);
    }

    return mapOfOriginalAssessmentModeKeysAndImportedAndSavedAssessmentModes;
  }

  @SuppressWarnings("java:S3776")
  private void saveAssessmentModesGroupsAndAreasToDB(
      List<AssessmentModeGroupsAndAreasExportImport> assessmentModeGroupsAndAreasExportImportList,
      Map<Long, AssessmentMode> mapOfOriginalAssessmentModeKeyAndSavedAssessmentModes,
      RepositoryEntry importedRepositoryEntry,
      CourseGroupManager courseGroupManager) {

    for (AssessmentModeGroupsAndAreasExportImport assessmentModeGroupsAndAreasExportImport :
        assessmentModeGroupsAndAreasExportImportList) {

      // Get assessment mode with same key from list of previously saved assessment modes
      AssessmentMode savedAssessmentMode =
          mapOfOriginalAssessmentModeKeyAndSavedAssessmentModes.get(
              assessmentModeGroupsAndAreasExportImport.getAssessmentModeKey());
      if (savedAssessmentMode == null) {
        throw new AssessmentModeImportException(
            "Cannot save groups and areas for assessment mode with original id "
                + assessmentModeGroupsAndAreasExportImport.getAssessmentModeKey(),
            importedRepositoryEntry);
      }

      // Assessment mode groups cannot be saved directly, because the business group keys were
      // changed during saving of the business groups of the imported course. Since we do not know
      // the original business group keys for the imported business groups, we look for business
      // groups with the same name as the assessment mode groups and save these groups as assessment
      // mode groups.
      List<BusinessGroup> businessGroupsOfPreviouslyImportedAndSavedCourse =
          courseGroupManager.getAllBusinessGroups();

      for (AssessmentModeGroupsAndAreasExportImport.Group group :
          assessmentModeGroupsAndAreasExportImport.getGroups()) {

        List<BusinessGroup> matchingBusinessGroups =
            businessGroupsOfPreviouslyImportedAndSavedCourse.stream()
                .filter(businessGroup -> businessGroup.getName().equals(group.getName()))
                .distinct()
                .toList();

        if (matchingBusinessGroups.size() == 1) {
          assessmentModeManager.createAssessmentModeToGroup(
              savedAssessmentMode, matchingBusinessGroups.get(0));
        } else if (matchingBusinessGroups.isEmpty()) {
          throw new AssessmentModeImportException(
              "Cannot save business group '"
                  + group.getName()
                  + "' of assessment mode '"
                  + savedAssessmentMode.getName()
                  + "'.",
              importedRepositoryEntry);
        } else {
          throw new AssessmentModeGroupNameNotUniqueException(
              importedRepositoryEntry, savedAssessmentMode.getName(), group.getName());
        }
      }

      // Assessment mode areas cannot be saved directly, because the BG area keys were changed
      // during saving of the BG areas of the imported course. Since we do not know the original
      // area key for the imported BG areas, we look for BG areas with the same name as the
      // assessment mode areas and save these areas as assessment mode areas.
      List<BGArea> bgAreasOfPreviouslyImportedAndSavedCourse = courseGroupManager.getAllAreas();

      for (AssessmentModeGroupsAndAreasExportImport.Area area :
          assessmentModeGroupsAndAreasExportImport.getAreas()) {

        List<BGArea> matchingBgAreas =
            bgAreasOfPreviouslyImportedAndSavedCourse.stream()
                .filter(bgArea -> bgArea.getName().equals(area.getName()))
                .distinct()
                .toList();

        if (matchingBgAreas.size() == 1) {
          assessmentModeManager.createAssessmentModeToArea(
              savedAssessmentMode, matchingBgAreas.get(0));
        } else if (matchingBgAreas.isEmpty()) {
          throw new AssessmentModeImportException(
              "Cannot save business group area '"
                  + area.getName()
                  + "' of assessment mode '"
                  + savedAssessmentMode.getName()
                  + "'.",
              importedRepositoryEntry);
        } else {
          throw new AssessmentModeAreaNameNotUniqueException(
              importedRepositoryEntry, savedAssessmentMode.getName(), area.getName());
        }
      }
    }
  }
}
