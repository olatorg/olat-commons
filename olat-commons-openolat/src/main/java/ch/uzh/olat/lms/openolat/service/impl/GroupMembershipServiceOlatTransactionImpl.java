/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.dao.GroupMembershipDao;
import ch.uzh.olat.lms.openolat.data.dao.RepositoryEntryToGroupRelationDao;
import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.service.GroupMembershipService;
import java.util.Collection;
import java.util.List;
import org.olat.basesecurity.Group;
import org.olat.basesecurity.GroupMembershipInheritance;
import org.olat.basesecurity.GroupRoles;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.olat.group.BusinessGroup;
import org.olat.repository.model.RepositoryEntryToGroupRelation;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class GroupMembershipServiceOlatTransactionImpl extends GroupMembershipServiceTemplate
    implements GroupMembershipService {

  private final GroupMembershipDao groupMembershipDao;
  private final RepositoryEntryToGroupRelationDao repositoryEntryToGroupRelationDao;

  public GroupMembershipServiceOlatTransactionImpl(
      GroupMembershipDao groupMembershipDao,
      RepositoryEntryToGroupRelationDao repositoryEntryToGroupRelationDao) {
    this.groupMembershipDao = groupMembershipDao;
    this.repositoryEntryToGroupRelationDao = repositoryEntryToGroupRelationDao;
  }

  @Override
  public List<SimpleIdentity> getIdentitiesOfGroupMembersByBusinessGroupAndGroupRole(
      BusinessGroup businessGroup, GroupRoles groupRole) {
    return doGetIdentitiesOfGroupMembersByBusinessGroupAndGroupRole(businessGroup, groupRole);
  }

  @Override
  public List<SimpleIdentity>
      getIdentitiesOfMembersOfRepositoryEntryDefaultGroupByRepositoryEntryIdAndGroupRole(
          long repositoryEntryId, GroupRoles groupRole) {
    return doGetIdentitiesOfMembersOfRepositoryEntryDefaultGroupByRepositoryEntryIdAndGroupRole(
        repositoryEntryId, groupRole);
  }

  @Override
  public void addNewMemberToGroup(
      SimpleIdentity identity,
      Group group,
      String role,
      GroupMembershipInheritance groupMembershipInheritance) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addNewMembersToBusinessGroup(
      Collection<SimpleIdentity> simpleIdentities,
      BusinessGroup businessGroup,
      GroupRoles groupRole) {
    doAddNewMembersToBusinessGroup(simpleIdentities, businessGroup, groupRole);
  }

  @Override
  public void addNewMembersToRepositoryEntryDefaultGroup(
      Collection<SimpleIdentity> simpleIdentities, long repositoryEntryId, GroupRoles groupRole) {
    doAddNewMembersToRepositoryEntryDefaultGroup(simpleIdentities, repositoryEntryId, groupRole);
  }

  @Override
  public void removeMembersFromBusinessGroup(
      Collection<SimpleIdentity> simpleIdentities,
      BusinessGroup businessGroup,
      GroupRoles groupRole) {
    doRemoveMembersFromBusinessGroup(simpleIdentities, businessGroup, groupRole);
  }

  @Override
  protected List<GroupMembershipImpl> getGroupMembershipsByGroupKeyAndRole(
      long groupKey, String roleName) {
    return groupMembershipDao.findByGroupKeyAndRoleWithEagerLoading(groupKey, roleName);
  }

  @Override
  protected List<GroupMembershipImpl> getGroupMembershipsByGroupKeyAndIdentityKeysAndRole(
      Long groupKey, List<Long> identityKeys, String roleName) {
    return groupMembershipDao.findByGroupKeyAndIdentityKeyInAndRole(
        groupKey, identityKeys, roleName);
  }

  @Override
  protected void saveGroupMembership(GroupMembershipImpl groupMembershipImpl) {
    groupMembershipDao.save(groupMembershipImpl);
  }

  @Override
  protected void deleteGroupMembership(GroupMembershipImpl groupMembershipImpl) {
    groupMembershipDao.delete(groupMembershipImpl);
  }

  @Override
  protected List<RepositoryEntryToGroupRelation>
      getRepositoryEntryToGroupRelationsOfDefaultGroupByRepositoryEntryId(long repositoryEntryId) {
    return repositoryEntryToGroupRelationDao.findByRepositoryEntryIdAndDefaultGroup(
        repositoryEntryId, true);
  }
}
