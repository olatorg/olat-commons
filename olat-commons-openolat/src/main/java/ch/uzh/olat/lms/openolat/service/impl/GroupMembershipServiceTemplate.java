/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.olat.basesecurity.Group;
import org.olat.basesecurity.GroupMembershipInheritance;
import org.olat.basesecurity.GroupRoles;
import org.olat.basesecurity.IdentityImpl;
import org.olat.basesecurity.model.GroupMembershipImpl;
import org.olat.core.logging.OLATRuntimeException;
import org.olat.group.BusinessGroup;
import org.olat.repository.model.RepositoryEntryToGroupRelation;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public abstract class GroupMembershipServiceTemplate {

  private List<SimpleIdentity> doGetIdentitiesOfGroupMembersByGroupKeyAndGroupRoles(
      long groupKey, GroupRoles groupRoles) {

    List<GroupMembershipImpl> groupMemberships =
        getGroupMembershipsByGroupKeyAndRole(groupKey, groupRoles.name());

    return groupMemberships.stream()
        .map(groupMembership -> new SimpleIdentity(groupMembership.getIdentity()))
        .toList();
  }

  protected List<SimpleIdentity> doGetIdentitiesOfGroupMembersByBusinessGroupAndGroupRole(
      BusinessGroup businessGroup, GroupRoles groupRole) {
    Group group = businessGroup.getBaseGroup();
    return doGetIdentitiesOfGroupMembersByGroupKeyAndGroupRoles(group.getKey(), groupRole);
  }

  protected List<SimpleIdentity>
      doGetIdentitiesOfMembersOfRepositoryEntryDefaultGroupByRepositoryEntryIdAndGroupRole(
          long repositoryEntryId, GroupRoles groupRole) {
    Group group = getRepositoryEntryDefaultGroup(repositoryEntryId);
    if (group == null) {
      return new ArrayList<>();
    }
    return doGetIdentitiesOfGroupMembersByGroupKeyAndGroupRoles(group.getKey(), groupRole);
  }

  private Group getRepositoryEntryDefaultGroup(long repositoryEntryId) {
    List<RepositoryEntryToGroupRelation> repositoryEntryToGroupRelations =
        getRepositoryEntryToGroupRelationsOfDefaultGroupByRepositoryEntryId(repositoryEntryId);
    if (repositoryEntryToGroupRelations.isEmpty()) {
      throw new OLATRuntimeException(
          "No default group found for repository entry with id " + repositoryEntryId);
    } else if (repositoryEntryToGroupRelations.size() >= 2) {
      throw new OLATRuntimeException(
          "More than one default groups found for repository entry with id " + repositoryEntryId);
    }
    return repositoryEntryToGroupRelations.get(0).getGroup();
  }

  protected void doAddNewMemberToGroup(
      SimpleIdentity simpleIdentity,
      Group group,
      String role,
      GroupMembershipInheritance groupMembershipInheritance) {

    GroupMembershipImpl groupMembershipImpl = new GroupMembershipImpl();
    groupMembershipImpl.setGroup(group);
    IdentityImpl identity = new IdentityImpl();
    identity.setKey(simpleIdentity.getId());
    groupMembershipImpl.setIdentity(identity);
    groupMembershipImpl.setRole(role);
    groupMembershipImpl.setInheritanceMode(groupMembershipInheritance);
    Date now = new Date();
    groupMembershipImpl.setCreationDate(now);
    groupMembershipImpl.setLastModified(now);

    saveGroupMembership(groupMembershipImpl);
  }

  protected void doAddNewMembersToBusinessGroup(
      Collection<SimpleIdentity> simpleIdentities,
      BusinessGroup businessGroup,
      GroupRoles groupRole) {
    Group group = businessGroup.getBaseGroup();
    for (SimpleIdentity simpleIdentity : simpleIdentities) {
      doAddNewMemberToGroup(
          simpleIdentity, group, groupRole.name(), GroupMembershipInheritance.none);
    }
  }

  protected void doAddNewMembersToRepositoryEntryDefaultGroup(
      Collection<SimpleIdentity> simpleIdentities, long repositoryEntryId, GroupRoles groupRole) {

    Group group = getRepositoryEntryDefaultGroup(repositoryEntryId);
    if (group == null) {
      return;
    }

    for (SimpleIdentity simpleIdentity : simpleIdentities) {
      doAddNewMemberToGroup(
          simpleIdentity, group, groupRole.name(), GroupMembershipInheritance.none);
    }
  }

  protected void doRemoveMembersFromBusinessGroup(
      Collection<SimpleIdentity> simpleIdentities,
      BusinessGroup businessGroup,
      GroupRoles groupRole) {

    Group group = businessGroup.getBaseGroup();

    List<Long> identityKeys = simpleIdentities.stream().map(SimpleIdentity::getId).toList();

    List<GroupMembershipImpl> groupMembershipsToBeRemoved =
        getGroupMembershipsByGroupKeyAndIdentityKeysAndRole(
            group.getKey(), identityKeys, groupRole.name());
    for (GroupMembershipImpl groupMembershipToBeRemoved : groupMembershipsToBeRemoved) {
      deleteGroupMembership(groupMembershipToBeRemoved);
    }
  }

  protected abstract List<GroupMembershipImpl> getGroupMembershipsByGroupKeyAndRole(
      long groupKey, String roleName);

  protected abstract List<GroupMembershipImpl> getGroupMembershipsByGroupKeyAndIdentityKeysAndRole(
      Long groupKey, List<Long> identityKeys, String roleName);

  protected abstract void saveGroupMembership(GroupMembershipImpl groupMembershipImpl);

  protected abstract void deleteGroupMembership(GroupMembershipImpl groupMembershipImpl);

  protected abstract List<RepositoryEntryToGroupRelation>
      getRepositoryEntryToGroupRelationsOfDefaultGroupByRepositoryEntryId(long repositoryEntryId);
}
