/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.ui.flexitable;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiCellRenderer;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableComponent;
import org.olat.core.gui.render.Renderer;
import org.olat.core.gui.render.StringOutput;
import org.olat.core.gui.render.URLBuilder;
import org.olat.core.gui.translator.Translator;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("unused") // used by olat-campuskurs
public class TimestampFlexiCellRenderer implements FlexiCellRenderer {

  @Override
  public void render(
      Renderer renderer,
      StringOutput target,
      Object cellValue,
      int row,
      FlexiTableComponent source,
      URLBuilder ubu,
      Translator translator) {
    if (cellValue instanceof Date date) {
      String formattedDate = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
      target.append(formattedDate);
    } else if (cellValue instanceof LocalDateTime localDateTime) {
      String formattedDate =
          localDateTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
      target.append(formattedDate);
    }
  }
}
