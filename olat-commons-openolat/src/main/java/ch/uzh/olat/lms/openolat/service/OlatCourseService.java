/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service;

import ch.uzh.olat.lms.openolat.exception.UnknownCourseDesignException;
import ch.uzh.olat.lms.openolat.exception.UnknownCourseTypeException;
import ch.uzh.olat.lms.openolat.model.CourseDesign;
import ch.uzh.olat.lms.openolat.model.CourseType;
import java.util.List;
import java.util.Locale;
import org.olat.core.id.Identity;
import org.olat.core.id.Organisation;
import org.olat.core.id.Roles;
import org.olat.repository.ErrorList;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryStatusEnum;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface OlatCourseService {

  @SuppressWarnings({"java:S107", "unused"}) // used by olat-campuskurs
  RepositoryEntry copyCourse(
      RepositoryEntry repositoryEntryOrig,
      String displayName,
      String shortTitle,
      RepositoryEntryStatusEnum status,
      String managedFlags,
      List<Organisation> assignedOrganisations,
      List<Organisation> organisationsToCreateWithoutBookingOffer,
      Identity identity);

  @SuppressWarnings("unused") // used by olat-campuskurs
  ErrorList permanentlyDeleteCourse(
      RepositoryEntry repositoryEntry, Identity identity, Roles roles, Locale locale);

  @SuppressWarnings("unused") // used by olat-campuskurs
  CourseType getCourseType(RepositoryEntry repositoryEntry) throws UnknownCourseTypeException;

  CourseType getCourseTypeFromCourseConfig(RepositoryEntry repositoryEntry)
      throws UnknownCourseTypeException;

  @SuppressWarnings("unused") // used by olat-campuskurs
  CourseDesign getCourseDesign(RepositoryEntry repositoryEntry) throws UnknownCourseDesignException;

  @SuppressWarnings("unused") // used by olat-campuskurs
  CourseDesign getDefaultCourseDesign();

  @SuppressWarnings("unused") // used by olat-campuskurs
  void changeCourseDesignOfLearningPathCourseTo(
      RepositoryEntry repositoryEntry, CourseDesign newCourseDesign)
      throws UnknownCourseDesignException;
}
