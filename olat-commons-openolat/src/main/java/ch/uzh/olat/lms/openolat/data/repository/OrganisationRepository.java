/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import java.util.List;
import java.util.Optional;
import org.olat.basesecurity.model.OrganisationImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Repository
public interface OrganisationRepository extends JpaRepository<OrganisationImpl, Long> {

  Optional<OrganisationImpl> findByIdentifier(String identifier);

  @Query(
      "SELECT org FROM organisation org "
          + "INNER JOIN FETCH org.group baseGroup "
          + "WHERE org.materializedPathKeys LIKE CONCAT(:materializedPathKeys,'%') "
          + "AND NOT (org.key = :organisationKey) "
          + "AND org.status IN :notDeletedStatuses")
  List<OrganisationImpl> findDescendants(
      @Param("organisationKey") long organisationKey,
      @Param("materializedPathKeys") String materializedPathKeys,
      @Param("notDeletedStatuses") List<String> notDeletedStatuses);
}
