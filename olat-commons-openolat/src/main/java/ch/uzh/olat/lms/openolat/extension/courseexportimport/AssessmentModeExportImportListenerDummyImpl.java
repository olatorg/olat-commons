/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.extension.courseexportimport;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import org.olat.course.assessment.AssessmentMode;
import org.olat.repository.RepositoryEntry;
import org.springframework.stereotype.Component;

/**
 * In order the event listener array is never null, one listener must exist.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Component
public class AssessmentModeExportImportListenerDummyImpl
    implements AssessmentModeExportImportListener {

  @Override
  public void onAfterExport(
      List<AssessmentMode> assessmentModes,
      RepositoryEntry repositoryEntry,
      ZipOutputStream zipOutputStream) {
    // NOOP
  }

  @Override
  public void onAfterImport(
      Map<Long, AssessmentMode> mapOfOriginalAssessmentModeKeyAndSavedAssessmentModes,
      RepositoryEntry importedRepositoryEntry,
      File fImportBaseDirectory) {
    // NOOP
  }
}
