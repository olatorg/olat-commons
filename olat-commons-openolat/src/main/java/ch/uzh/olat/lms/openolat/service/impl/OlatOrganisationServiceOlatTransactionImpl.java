/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.dao.OrganisationDao;
import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.service.OlatOrganisationService;
import java.util.Optional;
import org.olat.basesecurity.OrganisationRoles;
import org.olat.basesecurity.model.OrganisationImpl;
import org.olat.core.id.Organisation;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 4.0
 */
@Service
public class OlatOrganisationServiceOlatTransactionImpl extends OlatOrganisationServiceTemplate
    implements OlatOrganisationService {

  private final OrganisationDao organisationDao;

  public OlatOrganisationServiceOlatTransactionImpl(OrganisationDao organisationDao) {
    this.organisationDao = organisationDao;
  }

  @Override
  public Organisation getDefaultOrganisation() {
    return doGetDefaultOrganisation();
  }

  @Override
  public Optional<OrganisationImpl> getUzhOrganisation() {
    return doGetUzhOrganisation();
  }

  @Override
  public void addNewMemberToOrganisationIfNotExistingYet(
      SimpleIdentity identity, Organisation organisation, OrganisationRoles organisationRole) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected Optional<OrganisationImpl> findByIdentifier(String identifier) {
    return organisationDao.findByIdentifier(identifier);
  }
}
