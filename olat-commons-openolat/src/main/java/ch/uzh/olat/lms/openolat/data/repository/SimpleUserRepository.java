/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.repository;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Repository
public interface SimpleUserRepository extends JpaRepository<SimpleUser, Long> {

  @Query("SELECT su FROM SimpleUser su WHERE su.simpleIdentity.id = :identityId")
  Optional<SimpleUser> findByIdentityId(@Param("identityId") long identityId);

  @SuppressWarnings("unused") // used by olat-peer-review
  @Query("SELECT su FROM SimpleUser su WHERE su.simpleIdentity.id IN :identityIds")
  List<SimpleUser> findByIdentityIds(@Param("identityIds") List<Long> identityId);

  @Query("SELECT su.nickname FROM SimpleUser su WHERE su.simpleIdentity.id = :identityId")
  Optional<String> findNicknameByIdentityId(@Param("identityId") long identityId);

  @Query(
      "SELECT su FROM SimpleUser su "
          + "WHERE su.institutionalUserIdentifier LIKE :institutionalUserIdentifier "
          + "AND su.institutionalName = :institutionalName")
  List<SimpleUser> findByInstitutionalUserIdentifierLikeAndInstitutionalName(
      @Param("institutionalUserIdentifier") String institutionalUserIdentifier,
      @Param("institutionalName") String institutionalName);

  @Query(
      "SELECT su FROM SimpleUser su "
          + "WHERE (su.institutionalUserIdentifier LIKE :institutionalUserIdentifier "
          + "OR su.email = :email) "
          + "AND su.institutionalName = :institutionalName")
  List<SimpleUser> findByInstitutionalUserIdentifierLikeOrEmailAndInstitutionalName(
      @Param("institutionalUserIdentifier") String institutionalUserIdentifier,
      @Param("email") String email,
      @Param("institutionalName") String institutionalName);

  @Query(
      "SELECT su FROM SimpleUser su "
          + "WHERE (su.employeeNumber LIKE :employeeNumber "
          + "OR su.employeeNumber IN :employeeNumbers "
          + "OR su.email IN :emails) "
          + "AND su.institutionalName = :institutionalName")
  List<SimpleUser> findByEmployeeNumberLikeOrInOrEmailInAndInstitutionalName(
      @Param("employeeNumber") String employeeNumber,
      @Param("employeeNumbers") List<String> employeeNumbers,
      @Param("emails") List<String> emails,
      @Param("institutionalName") String institutionalName);

  @Query(
      "SELECT su FROM SimpleUser su "
          + "WHERE (su.employeeNumber LIKE :employeeNumber "
          + "OR su.employeeNumber IN :employeeNumbers) "
          + "AND su.institutionalName = :institutionalName")
  List<SimpleUser> findByEmployeeNumberLikeOrInAndInstitutionalName(
      @Param("employeeNumber") String employeeNumber,
      @Param("employeeNumbers") List<String> employeeNumbers,
      @Param("institutionalName") String institutionalName);

  @Query(
      "SELECT su FROM SimpleUser su "
          + "WHERE (su.employeeNumber LIKE :employeeNumber "
          + "OR su.email IN :emails) "
          + "AND su.institutionalName = :institutionalName")
  List<SimpleUser> findByEmployeeNumberLikeOrEmailInAndInstitutionalName(
      @Param("employeeNumber") String employeeNumber,
      @Param("emails") List<String> emails,
      @Param("institutionalName") String institutionalName);

  @Query(
      "SELECT su FROM SimpleUser su "
          + "WHERE su.employeeNumber LIKE :employeeNumber "
          + "AND su.institutionalName = :institutionalName")
  List<SimpleUser> findByEmployeeNumberLikeAndInstitutionalName(
      @Param("employeeNumber") String employeeNumber,
      @Param("institutionalName") String institutionalName);

  @SuppressWarnings("JpaQlInspection")
  @Query(
      "SELECT new ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname(su.simpleIdentity.id, su.nickname) "
          + "FROM SimpleUser su "
          + "WHERE su.simpleIdentity.id IN (:identityIds)")
  List<IdentityIdAndNickname> findIdentityIdAndNicknamesByIdentityIdIn(
      @Param("identityIds") List<Long> identityIds);
}
