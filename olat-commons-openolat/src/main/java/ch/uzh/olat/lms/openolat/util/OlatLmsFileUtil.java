/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.util.WebappHelper;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings({"unused", "LoggingSimilarMessage"}) // used by olat-seb-extension
@Slf4j
public class OlatLmsFileUtil {

  private OlatLmsFileUtil() {}

  private static final String TEMP_ROOT_DIRECTORY = "tmp_olat";
  private static final String DELIMITER = "/";

  public static <T> File getUserSpecificTempDirectory(
      Class<T> callingController,
      UserRequest userRequest,
      String additionalIdentifierForTempDirectory) {

    if (additionalIdentifierForTempDirectory == null
        || additionalIdentifierForTempDirectory.isBlank()) {
      return new File(
          WebappHelper.getTmpDir(),
          TEMP_ROOT_DIRECTORY
              + DELIMITER
              + callingController.getName()
              + DELIMITER
              + userRequest.getUserSession().getIdentity().getKey());
    } else {
      return new File(
          WebappHelper.getTmpDir(),
          TEMP_ROOT_DIRECTORY
              + DELIMITER
              + callingController.getName()
              + DELIMITER
              + userRequest.getUserSession().getIdentity().getKey()
              + "_"
              + additionalIdentifierForTempDirectory);
    }
  }

  @SuppressWarnings("java:S899")
  public static File writeByteArrayToFile(
      byte[] fileContentByteArray, File fileParentDirectory, String fileName) {

    // Create parent directory if required
    if (!fileParentDirectory.exists()) {
      boolean created = fileParentDirectory.mkdirs();
      if (!created) {
        log.error("Could not create {}", fileParentDirectory);
        return null;
      }
    }

    // Re(create) file
    File file = new File(fileParentDirectory, fileName);
    Path path = file.toPath();
    try {
      Files.deleteIfExists(path);
    } catch (IOException e) {
      log.error("Could not delete {}: {}", file, e.getMessage());
      return null;
    }
    try {
      //noinspection ResultOfMethodCallIgnored
      file.createNewFile();
    } catch (IOException e) {
      log.error("Could not create {}: {}", file, e.getMessage());
      return null;
    }

    // Write file content to file
    try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
      fileOutputStream.write(fileContentByteArray);
    } catch (IOException e) {
      log.error(e.getMessage());
      return null;
    }
    return file;
  }
}
