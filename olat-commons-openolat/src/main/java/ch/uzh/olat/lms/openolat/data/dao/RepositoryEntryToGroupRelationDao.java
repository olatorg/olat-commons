/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import java.util.List;
import org.olat.core.commons.persistence.DB;
import org.olat.repository.model.RepositoryEntryToGroupRelation;
import org.springframework.stereotype.Component;

/**
 * DAO for DB queries within OLAT transactions.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Component
public class RepositoryEntryToGroupRelationDao {

  private final DB dbInstance;

  public RepositoryEntryToGroupRelationDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public List<RepositoryEntryToGroupRelation> findByRepositoryEntryIdAndDefaultGroup(
      long repositoryEntryKey, boolean defaultGroup) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT repoentrytogroup FROM repoentrytogroup repoentrytogroup "
                + "LEFT JOIN FETCH repoentrytogroup.group "
                + "LEFT JOIN FETCH repoentrytogroup.entry "
                + "WHERE repoentrytogroup.entry.key = :repositoryEntryKey "
                + "AND repoentrytogroup.defaultGroup = :defaultGroup",
            RepositoryEntryToGroupRelation.class)
        .setParameter("repositoryEntryKey", repositoryEntryKey)
        .setParameter("defaultGroup", defaultGroup)
        .getResultList();
  }
}
