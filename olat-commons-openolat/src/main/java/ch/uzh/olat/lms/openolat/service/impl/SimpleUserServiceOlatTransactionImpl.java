/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.dao.SimpleUserDao;
import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import ch.uzh.olat.lms.openolat.service.SimpleUserService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.olat.core.id.Identity;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class SimpleUserServiceOlatTransactionImpl extends SimpleUserServiceTemplate
    implements SimpleUserService {

  private final SimpleUserDao simpleUserDao;

  public SimpleUserServiceOlatTransactionImpl(SimpleUserDao simpleUserDao) {
    this.simpleUserDao = simpleUserDao;
  }

  @Override
  public Optional<SimpleUser> getSimpleUser(SimpleIdentity simpleIdentity) {
    return simpleUserDao.findByIdentityId(simpleIdentity.getId());
  }

  @Override
  public String getUserName(Identity identity) {
    return doGetUserName(identity);
  }

  @Override
  public Map<Long, String> getMapOfIdentityIdsAndNotEmptyNicknamesByIdentityIds(
      List<Long> identityIds) {
    return doGetMapOfIdentityIdsAndNotEmptyNicknamesByIdentityIds(identityIds);
  }

  @Override
  protected Optional<String> getNicknameByIdentityId(long identityId) {
    return simpleUserDao.findNicknameByIdentityId(identityId);
  }

  @Override
  protected List<IdentityIdAndNickname> getIdentityIdAndNicknamesByIdentityIdIn(
      List<Long> identityIds) {
    return simpleUserDao.findIdentityIdAndNicknamesByIdentityIdIn(identityIds);
  }
}
