/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.ui.util;

import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.IntegerElement;
import org.olat.core.gui.components.form.flexible.elements.TextElement;
import org.olat.core.gui.components.form.flexible.impl.Form;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.translator.Translator;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("unused") // used by olat-campuskurs
public class FormUtils {

  private FormUtils() {}

  public static FormLayoutContainer createDefaultFormLayoutContainer(
      FormItemContainer formItemContainer,
      Form mainForm,
      Translator translator,
      String formLayoutContainerName,
      String formLayoutContainerTitle,
      String formLayoutContainerDescription) {

    FormLayoutContainer formLayoutContainer =
        FormLayoutContainer.createDefaultFormLayout(formLayoutContainerName, translator);
    return setupFormLayoutContainer(
        formLayoutContainer,
        formItemContainer,
        mainForm,
        formLayoutContainerTitle,
        formLayoutContainerDescription);
  }

  public static FormLayoutContainer createVerticalFormLayoutContainer(
      FormItemContainer formItemContainer,
      Form mainForm,
      Translator translator,
      String formLayoutContainerName,
      String formLayoutContainerTitle,
      String formLayoutContainerDescription) {

    FormLayoutContainer formLayoutContainer =
        FormLayoutContainer.createVerticalFormLayout(formLayoutContainerName, translator);
    return setupFormLayoutContainer(
        formLayoutContainer,
        formItemContainer,
        mainForm,
        formLayoutContainerTitle,
        formLayoutContainerDescription);
  }

  private static FormLayoutContainer setupFormLayoutContainer(
      FormLayoutContainer formLayoutContainer,
      FormItemContainer formItemContainer,
      Form mainForm,
      String formLayoutContainerTitle,
      String formLayoutContainerDescription) {
    formLayoutContainer.setFormTitle(formLayoutContainerTitle);
    if (formLayoutContainerDescription != null) {
      formLayoutContainer.setFormDescription(formLayoutContainerDescription);
    }
    formItemContainer.add(formLayoutContainer);
    formLayoutContainer.setRootForm(mainForm);
    return formLayoutContainer;
  }

  public static int getNullIntegerAsZero(Integer integerValue) {
    return (integerValue == null ? 0 : integerValue);
  }

  public static String parseLongToStringNullSafe(Long longValue) {
    return (longValue == null ? null : Long.toString(longValue));
  }

  public static Long parseStringToLongNullSafe(String stringValue) {
    return ((stringValue == null || stringValue.isEmpty()) ? null : Long.parseLong(stringValue));
  }

  public static void trimTextElementValue(TextElement textElement) {
    String textElementValue = textElement.getValue();
    textElementValue = (textElementValue == null ? null : textElementValue.trim());
    textElement.setValue(textElementValue);
  }

  public static boolean checkIfStringValueChanged(
      TextElement textElement, String modelStringValue) {
    return textElement.getValue() != null && !textElement.getValue().equals(modelStringValue)
        || (textElement.getValue() == null && modelStringValue != null);
  }

  public static boolean checkIfIntValueChanged(
      IntegerElement integerElement, Integer modelIntegerValue) {
    return modelIntegerValue == null || (integerElement.getIntValue() != modelIntegerValue);
  }

  public static boolean checkIfLongValueChanged(TextElement textElement, Long modelLongValue) {
    Long textElementValueAsLong = parseStringToLongNullSafe(textElement.getValue());
    return textElementValueAsLong != null && !textElementValueAsLong.equals(modelLongValue)
        || (textElementValueAsLong == null && modelLongValue != null);
  }
}
