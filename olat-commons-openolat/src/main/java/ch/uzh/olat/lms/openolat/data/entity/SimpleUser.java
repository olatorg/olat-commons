/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import java.util.Objects;
import lombok.Getter;

/**
 * Simple implementation of OpenOLAT's o_bs_user table containing only a subset of the attributes.
 * This table is read-only (no setters).
 *
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("JpaQlInspection")
@Entity
@Table(name = "o_user")
@NamedQuery(
    name = SimpleUser.FIND_BY_IDENTITY_ID,
    query = "SELECT su FROM SimpleUser su WHERE su.simpleIdentity.id = :identityId")
@NamedQuery(
    name = SimpleUser.FIND_NICKNAME_BY_IDENTITY_ID,
    query = "SELECT su.nickname FROM SimpleUser su WHERE su.simpleIdentity.id = :identityId")
@NamedQuery(
    name = SimpleUser.FIND_IDENTITY_ID_AND_NICKNAMES_BY_IDENTITY_ID_IN,
    query =
        "SELECT new ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname(su.simpleIdentity.id, su.nickname) "
            + "FROM SimpleUser su "
            + "WHERE su.simpleIdentity.id IN (:identityIds)")
@Getter
public class SimpleUser {

  @Id
  @Column(name = "user_id")
  private long id;

  @Version int version;

  @OneToOne
  @JoinColumn(name = "fk_identity")
  private SimpleIdentity simpleIdentity;

  @Column(name = "u_firstname")
  private String firstName;

  @Column(name = "u_lastname")
  private String lastName;

  @Column(name = "u_email")
  private String email;

  @Column(name = "u_nickname")
  private String nickname;

  @Column(name = "u_institutionalname")
  private String institutionalName;

  @Column(name = "u_institutionaluseridentifier")
  private String institutionalUserIdentifier;

  @Column(name = "u_employeenumber")
  private String employeeNumber;

  public static final String FIND_BY_IDENTITY_ID = "SimpleUser.findByIdentityId";
  public static final String FIND_NICKNAME_BY_IDENTITY_ID = "SimpleUser.findNicknameByIdentityId";
  public static final String FIND_IDENTITY_ID_AND_NICKNAMES_BY_IDENTITY_ID_IN =
      "SimpleUser.findIdentityIdAndNicknamesByIdentityIdIn";

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SimpleUser that = (SimpleUser) o;
    return id == that.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
