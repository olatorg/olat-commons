/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import java.util.Optional;
import org.olat.basesecurity.OrganisationRoles;
import org.olat.basesecurity.model.OrganisationImpl;
import org.olat.core.id.Organisation;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface OlatOrganisationService {

  @SuppressWarnings("unused") // used by olat-campuskurs
  Organisation getDefaultOrganisation();

  @SuppressWarnings("unused") // used by olat-campuskurs
  Optional<OrganisationImpl> getUzhOrganisation();

  @SuppressWarnings("unused") // used by olat-campuskurs
  void addNewMemberToOrganisationIfNotExistingYet(
      SimpleIdentity identity, Organisation organisation, OrganisationRoles organisationRole);
}
