/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.extension.courseexportimport;

import java.io.File;
import java.util.zip.ZipOutputStream;
import org.olat.course.PersistingCourseImpl;
import org.olat.course.groupsandrights.CourseGroupManager;
import org.olat.extension.course.export.CourseExportImportListener;
import org.olat.repository.RepositoryEntry;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Component
public class CourseExportImportExtensionCourseExportImportListenerImpl
    implements CourseExportImportListener {

  private final CourseExportImportExtensionService courseExportImportExtensionService;

  public CourseExportImportExtensionCourseExportImportListenerImpl(
      CourseExportImportExtensionService courseExportImportExtensionService) {
    this.courseExportImportExtensionService = courseExportImportExtensionService;
  }

  @Override
  public void onAfterExport(PersistingCourseImpl sourceCourse, ZipOutputStream zipOutputStream) {
    courseExportImportExtensionService.exportAssessmentModes(sourceCourse, zipOutputStream);
  }

  @Override
  public void onAfterImport(
      RepositoryEntry importedRepositoryEntry,
      CourseGroupManager courseGroupManager,
      File fImportBaseDirectory) {
    courseExportImportExtensionService.importAssessmentModes(
        importedRepositoryEntry, courseGroupManager, fImportBaseDirectory);
  }
}
