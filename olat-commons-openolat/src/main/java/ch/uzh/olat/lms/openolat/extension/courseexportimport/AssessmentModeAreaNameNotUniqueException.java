/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.extension.courseexportimport;

import lombok.Getter;
import org.olat.repository.RepositoryEntry;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Getter
public class AssessmentModeAreaNameNotUniqueException extends RuntimeException {

  private final RepositoryEntry importedRepositoryEntry;
  private final String areaName;

  public AssessmentModeAreaNameNotUniqueException(
      RepositoryEntry importedRepositoryEntry, String assessmentModeName, String areaName) {
    super(
        "Error when trying to import assessment modes of course '"
            + importedRepositoryEntry.getDisplayname()
            + "'. The bg area '"
            + areaName
            + "' of the assessment mode '"
            + assessmentModeName
            + " could not be assigned, because there is more than one bg areas with this name in "
            + "the course. The course import was successful, but some of the assessment modes "
            + "are missing or erroneous!");
    this.importedRepositoryEntry = importedRepositoryEntry;
    this.areaName = areaName;
  }
}
