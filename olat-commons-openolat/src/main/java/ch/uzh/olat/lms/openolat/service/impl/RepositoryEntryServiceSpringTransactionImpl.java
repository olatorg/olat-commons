/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.repository.RepositoryEntryRepository;
import ch.uzh.olat.lms.openolat.model.CourseType;
import ch.uzh.olat.lms.openolat.service.OlatCourseService;
import ch.uzh.olat.lms.openolat.service.RepositoryEntryService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryStatusEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class RepositoryEntryServiceSpringTransactionImpl extends RepositoryEntryServiceTemplate
    implements RepositoryEntryService {

  private final RepositoryEntryRepository repositoryEntryRepository;

  public RepositoryEntryServiceSpringTransactionImpl(
      @Qualifier("olatCourseServiceSpringTransactionImpl") OlatCourseService olatCourseService,
      RepositoryEntryRepository repositoryEntryRepository) {
    super(olatCourseService);
    this.repositoryEntryRepository = repositoryEntryRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<RepositoryEntry>
      getRepositoryEntryOfTypeCourseByRepositoryEntryStatusInAndRepositoryEntryId(
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums, long repositoryEntryId) {
    return doGetRepositoryEntryOfTypeCourseByRepositoryEntryStatusInAndRepositoryEntryId(
        repositoryEntryStatusEnums, repositoryEntryId);
  }

  @Override
  @Transactional(readOnly = true)
  public List<RepositoryEntry>
      getRepositoryEntriesByCourseTypeAndRepositoryEntryStatusAndCourseOwnerIdentityId(
          CourseType courseType,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId) {
    return doGetRepositoryEntriesByCourseTypeAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
        courseType, repositoryEntryStatusEnums, courseOwnerIdentityId);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Long>
      getRepositoryEntryIdsByCourseTypeInAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
          List<CourseType> courseTypes,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId) {
    return doGetRepositoryEntryIdsByCourseTypeInAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
        courseTypes, repositoryEntryStatusEnums, courseOwnerIdentityId);
  }

  @Override
  public Map<String, String> getMapOfRepositoryEntryInitialAuthorsAndFullName(
      List<RepositoryEntry> repositoryEntries) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected Optional<RepositoryEntry>
      findRepositoryEntryByRepositoryEntryIdWithEagerLoadingOfOlatResource(long repositoryEntryId) {
    return repositoryEntryRepository.findByIdWithEagerLoadingOfOlatResource(repositoryEntryId);
  }

  @Override
  protected List<RepositoryEntry>
      findRepositoryEntriesByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
          String resName,
          String technicalType,
          List<String> statusList,
          String role,
          long identityId) {
    return repositoryEntryRepository.findByResNameAndTechnicalTypeAndStatusInAndRoleAndIdentityId(
        resName, technicalType, statusList, role, identityId);
  }

  @Override
  protected List<Long>
      findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
          String resName,
          List<String> technicalTypes,
          List<String> statusList,
          String role,
          long identityId) {
    return repositoryEntryRepository
        .findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
            resName, technicalTypes, statusList, role, identityId);
  }

  @Override
  protected List<RepositoryEntry>
      findRepositoryEntriesByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
          String resName, List<String> statusList, String role, long identityId) {
    return repositoryEntryRepository
        .findByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
            resName, statusList, role, identityId);
  }
}
