/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import jakarta.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
import org.olat.core.commons.persistence.DB;
import org.olat.repository.RepositoryEntry;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 * DAO for DB queries within OLAT transactions.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Component("olatlmsRepositoryEntryDao")
public class RepositoryEntryDao {

  private static final String ID = "id";
  private static final String IDENTITY_ID = "identityId";
  private static final String RES_NAME = "resName";
  private static final String ROLE = "role";
  private static final String STATUS_LIST = "statusList";
  private static final String TECHNICAL_TYPE = "technicalType";
  private static final String TECHNICAL_TYPES = "technicalTypes";

  private final DB dbInstance;

  public RepositoryEntryDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  @SuppressWarnings("unused") // used by olat-campuskurs
  public Optional<RepositoryEntry> findById(long id) {
    RepositoryEntry repositoryEntry =
        dbInstance.getCurrentEntityManager().find(RepositoryEntry.class, id);
    return (repositoryEntry == null) ? Optional.empty() : Optional.of(repositoryEntry);
  }

  public Optional<RepositoryEntry> findByIdWithEagerLoadingOfOlatResource(long id) {
    try {
      return Optional.of(
          dbInstance
              .getCurrentEntityManager()
              .createQuery(
                  "SELECT re FROM repositoryentry re "
                      + "LEFT JOIN FETCH re.olatResource "
                      + "WHERE re.key = :id",
                  RepositoryEntry.class)
              .setParameter("id", id)
              .getSingleResult());
    } catch (NoResultException | EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  public Optional<RepositoryEntry> findByIdWithEagerLoadingOfRepositoryEntryLifecycle(long id) {
    try {
      return Optional.of(
          dbInstance
              .getCurrentEntityManager()
              .createQuery(
                  "SELECT re FROM repositoryentry re "
                      + "LEFT JOIN FETCH re.lifecycle "
                      + "WHERE re.key = :id",
                  RepositoryEntry.class)
              .setParameter(ID, id)
              .getSingleResult());
    } catch (NoResultException | EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  public List<RepositoryEntry> findByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
      String resName, String technicalType, List<String> statusList, String role, long identityId) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT DISTINCT re FROM repositoryentry re "
                + "JOIN re.groups retogroup "
                + "ON retogroup.defaultGroup = true "
                + "JOIN retogroup.group bsgroup "
                + "JOIN bsgroup.members membership "
                + "ON membership.role = :role "
                + "WHERE membership.identity.key = :identityId "
                + "AND re.technicalType = :technicalType "
                + "AND re.olatResource.resName = :resName "
                + "AND re.status IN (:statusList)",
            RepositoryEntry.class)
        .setParameter(RES_NAME, resName)
        .setParameter(TECHNICAL_TYPE, technicalType)
        .setParameter(STATUS_LIST, statusList)
        .setParameter(ROLE, role)
        .setParameter(IDENTITY_ID, identityId)
        .getResultList();
  }

  public List<Long> findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
      String resName,
      List<String> technicalTypes,
      List<String> statusList,
      String role,
      long identityId) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT DISTINCT re.key FROM repositoryentry re "
                + "JOIN re.groups retogroup "
                + "ON retogroup.defaultGroup = true "
                + "JOIN retogroup.group bsgroup "
                + "JOIN bsgroup.members membership "
                + "ON membership.role = :role "
                + "WHERE membership.identity.key = :identityId "
                + "AND re.technicalType IN (:technicalTypes) "
                + "AND re.olatResource.resName = :resName "
                + "AND re.status IN (:statusList)",
            Long.class)
        .setParameter(RES_NAME, resName)
        .setParameter(TECHNICAL_TYPES, technicalTypes)
        .setParameter(STATUS_LIST, statusList)
        .setParameter(ROLE, role)
        .setParameter(IDENTITY_ID, identityId)
        .getResultList();
  }

  public List<RepositoryEntry> findByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
      String resName, List<String> statusList, String role, long identityId) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT DISTINCT re FROM repositoryentry re "
                + "JOIN re.groups retogroup "
                + "ON retogroup.defaultGroup = true "
                + "JOIN retogroup.group bsgroup "
                + "JOIN bsgroup.members membership "
                + "ON membership.role = :role "
                + "WHERE membership.identity.key = :identityId "
                + "AND re.technicalType IS NULL "
                + "AND re.olatResource.resName = :resName "
                + "AND re.status IN (:statusList)",
            RepositoryEntry.class)
        .setParameter(RES_NAME, resName)
        .setParameter(STATUS_LIST, statusList)
        .setParameter(ROLE, role)
        .setParameter(IDENTITY_ID, identityId)
        .getResultList();
  }
}
