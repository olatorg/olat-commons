/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import java.util.Optional;
import org.olat.core.commons.persistence.DB;
import org.springframework.stereotype.Component;

/**
 * DAO for DB queries within OLAT transactions.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Component
public class SimpleIdentityDao {

  private final DB dbInstance;

  public SimpleIdentityDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public Optional<SimpleIdentity> findById(long id) {
    SimpleIdentity simpleIdentity =
        dbInstance.getCurrentEntityManager().find(SimpleIdentity.class, id);
    return (simpleIdentity == null) ? Optional.empty() : Optional.of(simpleIdentity);
  }
}
