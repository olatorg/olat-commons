/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service;

import ch.uzh.olat.lms.openolat.model.CourseType;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryStatusEnum;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface RepositoryEntryService {

  @SuppressWarnings("unused") // used by olat-campuskurs
  Optional<RepositoryEntry>
      getRepositoryEntryOfTypeCourseByRepositoryEntryStatusInAndRepositoryEntryId(
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums, long repositoryEntryId);

  @SuppressWarnings("unused") // used by olat-campuskurs
  List<RepositoryEntry>
      getRepositoryEntriesByCourseTypeAndRepositoryEntryStatusAndCourseOwnerIdentityId(
          CourseType courseType,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId);

  @SuppressWarnings("unused") // used by olat-campuskurs
  List<Long> getRepositoryEntryIdsByCourseTypeInAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
      List<CourseType> courseTypes,
      List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
      long courseOwnerIdentityId);

  @SuppressWarnings("unused") // used by olat-campuskurs
  Map<String, String> getMapOfRepositoryEntryInitialAuthorsAndFullName(
      List<RepositoryEntry> repositoryEntries);
}
