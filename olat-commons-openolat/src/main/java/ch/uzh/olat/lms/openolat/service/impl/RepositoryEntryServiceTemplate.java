/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.exception.UnknownCourseTypeException;
import ch.uzh.olat.lms.openolat.model.CourseType;
import ch.uzh.olat.lms.openolat.service.OlatCourseService;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.olat.basesecurity.GroupRoles;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryStatusEnum;
import org.olat.resource.OLATResource;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("LoggingSimilarMessage")
@Slf4j
public abstract class RepositoryEntryServiceTemplate {

  private static final String COURSE_MODULE_RESOURCEABLE_TYPE_NAME = "CourseModule";

  private final OlatCourseService olatCourseService;

  protected RepositoryEntryServiceTemplate(OlatCourseService olatCourseService) {
    this.olatCourseService = olatCourseService;
  }

  protected Optional<RepositoryEntry>
      doGetRepositoryEntryOfTypeCourseByRepositoryEntryStatusInAndRepositoryEntryId(
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums, long repositoryEntryId) {

    Optional<RepositoryEntry> repositoryEntryOptional =
        findRepositoryEntryByRepositoryEntryIdWithEagerLoadingOfOlatResource(repositoryEntryId);

    List<String> repositoryEntryStatuses =
        repositoryEntryStatusEnums.stream().map(RepositoryEntryStatusEnum::toString).toList();
    if (repositoryEntryOptional.isEmpty()
        || !repositoryEntryStatuses.contains(repositoryEntryOptional.get().getStatus())) {
      return Optional.empty();
    }

    OLATResource olatResource = repositoryEntryOptional.get().getOlatResource();
    if (olatResource == null
        || !olatResource.getResourceableTypeName().equals(COURSE_MODULE_RESOURCEABLE_TYPE_NAME)) {
      return Optional.empty();
    }

    return repositoryEntryOptional;
  }

  protected List<RepositoryEntry>
      doGetRepositoryEntriesByCourseTypeAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
          CourseType courseType,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId) {

    List<String> repositoryEntryStatuses =
        repositoryEntryStatusEnums.stream()
            .distinct()
            .map(RepositoryEntryStatusEnum::toString)
            .toList();

    List<RepositoryEntry> repositoryEntries =
        findRepositoryEntriesByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
            COURSE_MODULE_RESOURCEABLE_TYPE_NAME,
            courseType.getTechnicalType(),
            repositoryEntryStatuses,
            GroupRoles.owner.name(),
            courseOwnerIdentityId);

    // Unfortunately, the technical type is nullable on the database, so we cannot be sure that the
    // attribute is set for all courses. Therefore, we also look for courses with technical type
    // null and try to determine the course type from the course config.
    List<RepositoryEntry> repositoryEntriesWithTechnicalTypeNull =
        findRepositoryEntriesByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
            COURSE_MODULE_RESOURCEABLE_TYPE_NAME,
            repositoryEntryStatuses,
            GroupRoles.owner.name(),
            courseOwnerIdentityId);

    for (RepositoryEntry repositoryEntry : repositoryEntriesWithTechnicalTypeNull) {
      try {
        CourseType courseTypeFromCourseConfig =
            olatCourseService.getCourseTypeFromCourseConfig(repositoryEntry);
        if (courseTypeFromCourseConfig == courseType) {
          repositoryEntries.add(repositoryEntry);
        }
      } catch (UnknownCourseTypeException e) {
        log.error("{} -> course skipped!", e.getMessage());
      }
    }

    return repositoryEntries;
  }

  protected List<Long>
      doGetRepositoryEntryIdsByCourseTypeInAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
          List<CourseType> courseTypes,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId) {

    List<String> repositoryEntryStatuses =
        repositoryEntryStatusEnums.stream()
            .distinct()
            .map(RepositoryEntryStatusEnum::toString)
            .toList();
    List<String> technicalTypes = courseTypes.stream().map(CourseType::getTechnicalType).toList();

    List<Long> repositoryEntryIds =
        findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
            COURSE_MODULE_RESOURCEABLE_TYPE_NAME,
            technicalTypes,
            repositoryEntryStatuses,
            GroupRoles.owner.name(),
            courseOwnerIdentityId);

    List<RepositoryEntry> repositoryEntriesWithTechnicalTypeNull =
        findRepositoryEntriesByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
            COURSE_MODULE_RESOURCEABLE_TYPE_NAME,
            repositoryEntryStatuses,
            GroupRoles.owner.name(),
            courseOwnerIdentityId);

    for (RepositoryEntry repositoryEntry : repositoryEntriesWithTechnicalTypeNull) {
      try {
        CourseType courseTypeFromCourseConfig =
            olatCourseService.getCourseTypeFromCourseConfig(repositoryEntry);
        if (courseTypes.contains(courseTypeFromCourseConfig)) {
          repositoryEntryIds.add(repositoryEntry.getKey());
        }
      } catch (UnknownCourseTypeException e) {
        log.error("{} -> course skipped!", e.getMessage());
      }
    }

    return repositoryEntryIds;
  }

  protected abstract Optional<RepositoryEntry>
      findRepositoryEntryByRepositoryEntryIdWithEagerLoadingOfOlatResource(long repositoryEntryId);

  @SuppressWarnings("SameParameterValue")
  protected abstract List<RepositoryEntry>
      findRepositoryEntriesByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
          String resName,
          String technicalType,
          List<String> statusList,
          String role,
          long identityId);

  @SuppressWarnings("SameParameterValue")
  protected abstract List<Long>
      findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
          String resName,
          List<String> technicalTypes,
          List<String> statusList,
          String role,
          long identityId);

  @SuppressWarnings("SameParameterValue")
  protected abstract List<RepositoryEntry>
      findRepositoryEntriesByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
          String resName, List<String> statusList, String role, long identityId);
}
