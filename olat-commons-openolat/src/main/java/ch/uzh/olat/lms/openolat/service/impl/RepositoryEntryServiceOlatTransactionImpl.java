/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.service.impl;

import ch.uzh.olat.lms.openolat.data.dao.RepositoryEntryDao;
import ch.uzh.olat.lms.openolat.model.CourseType;
import ch.uzh.olat.lms.openolat.service.OlatCourseService;
import ch.uzh.olat.lms.openolat.service.RepositoryEntryService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryStatusEnum;
import org.olat.user.UserManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class RepositoryEntryServiceOlatTransactionImpl extends RepositoryEntryServiceTemplate
    implements RepositoryEntryService {

  private final UserManager userManager;
  private final RepositoryEntryDao repositoryEntryDao;

  /* Note: Due to several cyclic dependencies of the OpenOLAT beans, it's important that the
   * olat-lms beans don't affect the order in which the OpenOLAT beans are loaded. This can be
   * achieved by Spring's @Lazy annotation, which should always be used when autowiring OpenOLAT
   * services from olat-lms beans.
   */
  public RepositoryEntryServiceOlatTransactionImpl(
      @Qualifier("olatCourseServiceOlatTransactionImpl") OlatCourseService olatCourseService,
      @Lazy UserManager userManager,
      RepositoryEntryDao repositoryEntryRepository) {
    super(olatCourseService);
    this.userManager = userManager;
    this.repositoryEntryDao = repositoryEntryRepository;
  }

  @Override
  public Optional<RepositoryEntry>
      getRepositoryEntryOfTypeCourseByRepositoryEntryStatusInAndRepositoryEntryId(
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums, long repositoryEntryId) {
    return doGetRepositoryEntryOfTypeCourseByRepositoryEntryStatusInAndRepositoryEntryId(
        repositoryEntryStatusEnums, repositoryEntryId);
  }

  @Override
  public List<RepositoryEntry>
      getRepositoryEntriesByCourseTypeAndRepositoryEntryStatusAndCourseOwnerIdentityId(
          CourseType courseType,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId) {
    return doGetRepositoryEntriesByCourseTypeAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
        courseType, repositoryEntryStatusEnums, courseOwnerIdentityId);
  }

  @Override
  public List<Long>
      getRepositoryEntryIdsByCourseTypeInAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
          List<CourseType> courseTypes,
          List<RepositoryEntryStatusEnum> repositoryEntryStatusEnums,
          long courseOwnerIdentityId) {
    return doGetRepositoryEntryIdsByCourseTypeInAndRepositoryEntryStatusInAndCourseOwnerIdentityId(
        courseTypes, repositoryEntryStatusEnums, courseOwnerIdentityId);
  }

  @Override
  public Map<String, String> getMapOfRepositoryEntryInitialAuthorsAndFullName(
      List<RepositoryEntry> repositoryEntries) {

    Set<String> repositoryEntryInitialAuthors =
        repositoryEntries.stream()
            .filter(
                repositoryEntry ->
                    repositoryEntry.getInitialAuthor() != null
                        && !repositoryEntry.getInitialAuthor().isBlank())
            .map(RepositoryEntry::getInitialAuthor)
            .collect(Collectors.toSet());

    return (repositoryEntryInitialAuthors.isEmpty())
        ? new HashMap<>()
        : userManager.getUserDisplayNamesByUserName(repositoryEntryInitialAuthors);
  }

  @Override
  protected Optional<RepositoryEntry>
      findRepositoryEntryByRepositoryEntryIdWithEagerLoadingOfOlatResource(long repositoryEntryId) {
    return repositoryEntryDao.findByIdWithEagerLoadingOfOlatResource(repositoryEntryId);
  }

  @Override
  protected List<RepositoryEntry>
      findRepositoryEntriesByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
          String resName,
          String technicalType,
          List<String> statusList,
          String role,
          long identityId) {
    return repositoryEntryDao.findByResNameAndTechnicalTypeAndStatusAndRoleAndIdentityId(
        resName, technicalType, statusList, role, identityId);
  }

  @Override
  protected List<Long>
      findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
          String resName,
          List<String> technicalTypes,
          List<String> statusList,
          String role,
          long identityId) {
    return repositoryEntryDao
        .findRepositoryEntryIdsByResNameAndTechnicalTypeInAndStatusAndRoleAndIdentityId(
            resName, technicalTypes, statusList, role, identityId);
  }

  @Override
  protected List<RepositoryEntry>
      findRepositoryEntriesByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
          String resName, List<String> statusList, String role, long identityId) {
    return repositoryEntryDao.findByResNameAndTechnicalTypeIsNullAndStatusInAndRoleAndIdentityId(
        resName, statusList, role, identityId);
  }
}
