/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.extension.courseexportimport;

import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.olat.course.assessment.AssessmentMode;
import org.olat.course.assessment.AssessmentModeToArea;
import org.olat.course.assessment.AssessmentModeToGroup;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@RequiredArgsConstructor
@Getter
@SuppressWarnings(
    "ClassCanBeRecord") // Must not be converted to record, as Xstream does currently not support
// records with JDK 17 (https://github.com/x-stream/xstream/issues/210).
public class AssessmentModeGroupsAndAreasExportImport {

  private final long assessmentModeKey;
  private final List<Group> groups;
  private final List<Area> areas;

  @SuppressWarnings("java:S6204")
  public static AssessmentModeGroupsAndAreasExportImport of(AssessmentMode assessmentMode) {
    List<Group> groups =
        assessmentMode.getGroups().stream()
            .map(AssessmentModeToGroup::getBusinessGroup)
            .map(
                businessGroup -> new Group(businessGroup.getName(), businessGroup.getDescription()))
            .collect(Collectors.toList());
    List<Area> areas =
        assessmentMode.getAreas().stream()
            .map(AssessmentModeToArea::getArea)
            .map(area -> new Area(area.getName(), area.getDescription()))
            .collect(Collectors.toList());
    return new AssessmentModeGroupsAndAreasExportImport(assessmentMode.getKey(), groups, areas);
  }

  @RequiredArgsConstructor
  @Getter
  public static class Group {
    private final String name;
    private final String description;
  }

  @RequiredArgsConstructor
  @Getter
  public static class Area {
    private final String name;
    private final String description;
  }
}
