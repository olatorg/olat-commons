/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import java.util.Objects;
import lombok.Getter;
import org.olat.core.id.Identity;

/**
 * Simple implementation of OpenOLAT's o_bs_identity table containing only a subset of the
 * attributes. In particular, the implementation avoids the automatic loading of the o_bs_user entry
 * referenced by o_bs_identity, as it is the case for the OpenOLAT implementation. This table is
 * read-only (no setters).
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Entity
@Table(name = "o_bs_identity")
@Getter
@SuppressWarnings({"initialization.fields.uninitialized"})
public class SimpleIdentity {

  // Limit for visible identities, all identities with status < LIMIT will be listed in search etc.
  public static final Integer STATUS_VISIBLE_LIMIT = 100;

  @Id private long id;

  @Version int version;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "status")
  private Integer status;

  public SimpleIdentity() {}

  public SimpleIdentity(long id, String name, Integer status) {
    this.id = id;
    this.name = name;
    this.status = status;
  }

  public SimpleIdentity(Identity identity) {
    id = identity.getKey();
    name = identity.getName();
    status = identity.getStatus();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SimpleIdentity that = (SimpleIdentity) o;
    return id == that.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
