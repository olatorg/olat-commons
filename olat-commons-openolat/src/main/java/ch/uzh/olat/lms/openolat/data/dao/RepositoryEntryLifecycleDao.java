/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import org.olat.core.commons.persistence.DB;
import org.olat.repository.model.RepositoryEntryLifecycle;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Component("olatlmsRepositoryEntryLifecycleDao")
public class RepositoryEntryLifecycleDao {

  private final DB dbInstance;

  public RepositoryEntryLifecycleDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  @SuppressWarnings("unused") // used by olat-campuskurs
  public void saveAndFlush(RepositoryEntryLifecycle repositoryEntryLifecycle) {
    dbInstance.saveObject(repositoryEntryLifecycle);
    dbInstance.getCurrentEntityManager().flush();
  }

  @SuppressWarnings("unused") // used by olat-campuskurs
  public void delete(RepositoryEntryLifecycle repositoryEntryLifecycle) {
    dbInstance.deleteObject(repositoryEntryLifecycle);
  }
}
