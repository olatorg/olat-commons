/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.extension.courseexportimport;

import java.util.Date;
import lombok.Getter;
import org.olat.course.assessment.AssessmentMode;
import org.olat.course.assessment.model.AssessmentModeImpl;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public class AssessmentModeWithoutGroupsAndAreasExportImport {

  @Getter private final long key;
  private final Date creationDate;
  private final Date lastModified;
  private final String externalId;
  private final String managedFlagsString;

  private final String name;
  private final String description;
  private final String statusString;
  private final String endStatusString;
  private final Date begin;

  private final int leadTime;
  private final Date beginWithLeadTime;
  private final Date end;
  private final int followupTime;
  private final Date endWithFollowupTime;

  private final boolean manualBeginEnd;
  private final String targetAudienceString;
  private final boolean restrictAccessElements;
  private final String elementList;
  private final String startElement;

  private final boolean restrictAccessIps;
  private final String ipList;
  private final boolean safeExamBrowser;
  private final String safeExamBrowserKey;
  private final String safeExamBrowserConfigXml;

  private final String safeExamBrowserConfigPlist;
  private final String safeExamBrowserConfigPlistKey;
  private final boolean safeExamBrowserConfigDownload;
  private final String safeExamBrowserHint;
  private final boolean applySettingsForCoach;

  @SuppressWarnings("java:S107")
  public AssessmentModeWithoutGroupsAndAreasExportImport(
      long key,
      Date creationDate,
      Date lastModified,
      String externalId,
      String managedFlagsString,
      String name,
      String description,
      String statusString,
      String endStatusString,
      Date begin,
      int leadTime,
      Date beginWithLeadTime,
      Date end,
      int followupTime,
      Date endWithFollowupTime,
      boolean manualBeginEnd,
      String targetAudienceString,
      boolean restrictAccessElements,
      String elementList,
      String startElement,
      boolean restrictAccessIps,
      String ipList,
      boolean safeExamBrowser,
      String safeExamBrowserKey,
      String safeExamBrowserConfigXml,
      String safeExamBrowserConfigPlist,
      String safeExamBrowserConfigPlistKey,
      boolean safeExamBrowserConfigDownload,
      String safeExamBrowserHint,
      boolean applySettingsForCoach) {
    this.key = key;
    this.creationDate = creationDate;
    this.lastModified = lastModified;
    this.externalId = externalId;
    this.managedFlagsString = managedFlagsString;
    this.name = name;
    this.description = description;
    this.statusString = statusString;
    this.endStatusString = endStatusString;
    this.begin = begin;
    this.leadTime = leadTime;
    this.beginWithLeadTime = beginWithLeadTime;
    this.end = end;
    this.followupTime = followupTime;
    this.endWithFollowupTime = endWithFollowupTime;
    this.manualBeginEnd = manualBeginEnd;
    this.targetAudienceString = targetAudienceString;
    this.restrictAccessElements = restrictAccessElements;
    this.elementList = elementList;
    this.startElement = startElement;
    this.restrictAccessIps = restrictAccessIps;
    this.ipList = ipList;
    this.safeExamBrowser = safeExamBrowser;
    this.safeExamBrowserKey = safeExamBrowserKey;
    this.safeExamBrowserConfigXml = safeExamBrowserConfigXml;
    this.safeExamBrowserConfigPlist = safeExamBrowserConfigPlist;
    this.safeExamBrowserConfigPlistKey = safeExamBrowserConfigPlistKey;
    this.safeExamBrowserConfigDownload = safeExamBrowserConfigDownload;
    this.safeExamBrowserHint = safeExamBrowserHint;
    this.applySettingsForCoach = applySettingsForCoach;
  }

  public static AssessmentModeWithoutGroupsAndAreasExportImport of(
      AssessmentModeImpl assessmentModeImpl) {
    return new AssessmentModeWithoutGroupsAndAreasExportImport(
        assessmentModeImpl.getKey(),
        assessmentModeImpl.getCreationDate(),
        assessmentModeImpl.getLastModified(),
        assessmentModeImpl.getExternalId(),
        assessmentModeImpl.getManagedFlagsString(),
        assessmentModeImpl.getName(),
        assessmentModeImpl.getDescription(),
        assessmentModeImpl.getStatusString(),
        assessmentModeImpl.getEndStatusString(),
        assessmentModeImpl.getBegin(),
        assessmentModeImpl.getLeadTime(),
        assessmentModeImpl.getBeginWithLeadTime(),
        assessmentModeImpl.getEnd(),
        assessmentModeImpl.getFollowupTime(),
        assessmentModeImpl.getEndWithFollowupTime(),
        assessmentModeImpl.isManualBeginEnd(),
        (assessmentModeImpl.getTargetAudience() == null)
            ? null
            : assessmentModeImpl.getTargetAudience().name(),
        assessmentModeImpl.isRestrictAccessElements(),
        assessmentModeImpl.getElementList(),
        assessmentModeImpl.getStartElement(),
        assessmentModeImpl.isRestrictAccessIps(),
        assessmentModeImpl.getIpList(),
        assessmentModeImpl.isSafeExamBrowser(),
        assessmentModeImpl.getSafeExamBrowserKey(),
        assessmentModeImpl.getSafeExamBrowserConfigXml(),
        assessmentModeImpl.getSafeExamBrowserConfigPList(),
        assessmentModeImpl.getSafeExamBrowserConfigPListKey(),
        assessmentModeImpl.isSafeExamBrowserConfigDownload(),
        assessmentModeImpl.getSafeExamBrowserHint(),
        assessmentModeImpl.isApplySettingsForCoach());
  }

  public AssessmentModeImpl toAssessmentModeImpl() {
    AssessmentModeImpl assessmentModeImpl = new AssessmentModeImpl();

    assessmentModeImpl.setKey(key);
    assessmentModeImpl.setCreationDate(creationDate);
    assessmentModeImpl.setLastModified(lastModified);
    assessmentModeImpl.setExternalId(externalId);
    assessmentModeImpl.setManagedFlagsString(managedFlagsString);

    assessmentModeImpl.setName(name);
    assessmentModeImpl.setDescription(description);
    assessmentModeImpl.setStatusString(statusString);
    assessmentModeImpl.setEndStatusString(endStatusString);
    assessmentModeImpl.setBegin(begin);

    assessmentModeImpl.setLeadTime(leadTime);
    assessmentModeImpl.setBeginWithLeadTime(beginWithLeadTime);
    assessmentModeImpl.setEnd(end);
    assessmentModeImpl.setFollowupTime(followupTime);
    assessmentModeImpl.setEndWithFollowupTime(endWithFollowupTime);

    assessmentModeImpl.setManualBeginEnd(manualBeginEnd);
    assessmentModeImpl.setTargetAudience(
        (targetAudienceString == null || targetAudienceString.isEmpty())
            ? null
            : AssessmentMode.Target.valueOf(targetAudienceString));
    assessmentModeImpl.setRestrictAccessElements(restrictAccessElements);
    assessmentModeImpl.setElementList(elementList);
    assessmentModeImpl.setStartElement(startElement);

    assessmentModeImpl.setRestrictAccessIps(restrictAccessIps);
    assessmentModeImpl.setIpList(ipList);
    assessmentModeImpl.setSafeExamBrowser(safeExamBrowser);
    assessmentModeImpl.setSafeExamBrowserKey(safeExamBrowserKey);
    assessmentModeImpl.setSafeExamBrowserConfigXml(safeExamBrowserConfigXml);

    assessmentModeImpl.setSafeExamBrowserConfigPList(safeExamBrowserConfigPlist);
    assessmentModeImpl.setSafeExamBrowserConfigPListKey(safeExamBrowserConfigPlistKey);
    assessmentModeImpl.setSafeExamBrowserConfigDownload(safeExamBrowserConfigDownload);
    assessmentModeImpl.setSafeExamBrowserHint(safeExamBrowserHint);
    assessmentModeImpl.setApplySettingsForCoach(applySettingsForCoach);

    return assessmentModeImpl;
  }
}
