/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.lms.openolat.data.dao;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.model.IdentityIdAndNickname;
import jakarta.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
import org.olat.core.commons.persistence.DB;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 * DAO for DB queries within OLAT transactions.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Component
public class SimpleUserDao {

  private final DB dbInstance;

  public SimpleUserDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public Optional<SimpleUser> findByIdentityId(long identityId) {
    try {
      return Optional.of(
          dbInstance
              .getCurrentEntityManager()
              .createNamedQuery(SimpleUser.FIND_BY_IDENTITY_ID, SimpleUser.class)
              .setParameter("identityId", identityId)
              .getSingleResult());
    } catch (NoResultException | EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  public Optional<String> findNicknameByIdentityId(long identityId) {
    try {
      return Optional.of(
          dbInstance
              .getCurrentEntityManager()
              .createNamedQuery(SimpleUser.FIND_NICKNAME_BY_IDENTITY_ID, String.class)
              .setParameter("identityId", identityId)
              .getSingleResult());
    } catch (NoResultException | EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  public List<IdentityIdAndNickname> findIdentityIdAndNicknamesByIdentityIdIn(
      List<Long> identityIds) {
    return dbInstance
        .getCurrentEntityManager()
        .createNamedQuery(
            SimpleUser.FIND_IDENTITY_ID_AND_NICKNAMES_BY_IDENTITY_ID_IN,
            IdentityIdAndNickname.class)
        .setParameter("identityIds", identityIds)
        .getResultList();
  }
}
